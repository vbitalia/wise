<?php
/**
 * Wise Inquisitor interface file.
 *
 * Functions that WISE may need but not closely related to the algorithm.
 * The Wise Inquisitor validates arrays on behalf of Wise class.
 *
 * Copyright (C) 2015 VB Italia Srl
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  See LICENSE.md
 *
 * @package Wise\Lib
 * @copyright 2015 VB Italia Srl
 * @author Gianluca Merlo <gianluca.merlo@vbitalia.net>
 * @license GPLv2+
 * @since 0.5.0
 */

 namespace Wise;

 /**
  * Inquisitor interface.
  *
  * This interface defines the standards to work with the Inquisitor class.
  *
  * @package Wise\Lib
  * @author Gianluca Merlo <gianluca.merlo@vbitalia.net>
  * @since 0.5.0
  * @version 1.0.0
  */
  interface InquisitorInterface {
    /**
     * Check consistency of `'key' => value`.
     *
     * Check if the key passed is a string, and if the value passed is a number.
     * Also check if the value is positive, and non zero.
     *
     * @since 1.0.0
     * @access public
     * @static
     *
     * @param string $key Key of the array to check.
     * @param number $value Value of the array to check.
     * @return bool True if $key is a string and $value is a positive non zero
     * number, false otherwise.
     */
    public static function filterPositive($value, $key);

    /**
     * Check and filter `value`, if not in the correct form.
     *
     * Check if `value` passed is an array, and filter it by calling another
     * function to check whether it is in the correct form or not.
     * If `value` is not an array or if false is returned by the function called
     * this value is not in the correct form.
     *
     * @since 1.0.0
     * @access public
     * @static
     *
     * @param array $value Value of the array to check.
     * @param array $key Key of the array to check.
     * @return bool True if $value is in the correct form, false otherwise.
     */
    public static function filterArray($value, $key);

    /**
     * Check and filter `'key' => value`, if not in the correct form.
     *
     * Check if 'key' corresponds to a modifier, and if `value` is a positive
     * number or an array. If not, `'key' => value` will be removed from the
     * array.
     *
     * @since 1.0.0
     * @access public
     * @static
     *
     * @param string $key String corresponding to a modifier.
     * @param number|array $value Value to be sorted or the array with the value
     * and the priority.
     * @return bool True if $key and $value are in the correct form, false
     * otherwise.
     */
    public static function filterKeyNeedles($value, $key);

    /**
     * Check if `'key' => value` is in the correct form.
     *
     * Check if 'key' corresponds to "value" or "priority", and if value is
     * a number and, for "priority" only, if it's one of the constants allowed.
     *
     * @since 1.0.0
     * @access public
     * @static
     *
     * @param string $key The key that needs to be checked.
     * @param number|string The value that needs to be checked.
     * @return bool True if $key and $value are in the correct form, false
     * otherwise.
     */
    public static function filterValuePriority($value, $key);

    /**
     * Sanitize the array if it isn't in the correct form.
     *
     * Check if `value` is a number for the simple version, or an array for the
     * complex version.
     * If some values are not in the correct form they will be removed from the
     * array.
     *
     * @since 1.0.0
     * @access public
     * @static
     *
     * @param array $objects Array that needs to be sanitized.
     * @param array $needles If empty, check only if values are numeric.
     * If not empty, it will be used to store the needles to be searched for in
     * the inner array's keys.
     * Default empty.
     * @return array Contains keys and values of $objects that are in the
     * correct form.
     */
    public static function sanitizeArray(array $objects, $needles = array());
  }
