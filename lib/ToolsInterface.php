<?php
/**
 * Functions in support to WISE algorithm.
 *
 * Functions that WISE may need but not closely related to the algorithm.
 *
 * Copyright (C) 2015 VB Italia Srl
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  See LICENSE.md
 *
 * @package Wise\Lib
 * @copyright 2015 VB Italia Srl
 * @author Gianluca Merlo <gianluca.merlo@vbitalia.net>
 * @license GPLv2+
 * @since 0.5.0
 */

 namespace Wise;

 /**
  * Tools interface.
  *
  * This interface defines the standards to work with the Tools class.
  *
  * @package Wise\Lib
  * @author Gianluca Merlo <gianluca.merlo@vbitalia.net>
  * @since 0.5.0
  * @version 1.0.0
  */
  interface ToolsInterface {
    /**
     * Check path and decode JSON file.
     *
     * Check if the path is correct, if it correspond to a .json file, and if
     * the content is correct. Put the content of the file in an array.
     *
     * @since 1.0.0
     * @access public
     * @static
     *
     * @param string $path The path of the file.
     * @param int $depth Specified recursion depth level. Default null.
     * @return bool|array False if $path is not a string, the array with the
     * content of the file otherwise.
     *
     * @throws \InvalidArgumentException if $path is not a path to a JSON file.
     * @throws \RuntimeException if the file does not exist, or on failure in
     * reading or decoding file.
     */
    public static function getJson($path, $depth = null);
  }
