<?php
/**
 * Functions in support to WISE algorithm.
 *
 * Functions that WISE may need but not closely related to the algorithm.
 *
 * Copyright (C) 2015 VB Italia Srl
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  See LICENSE.md
 *
 * @package Wise\Lib
 * @copyright 2015 VB Italia Srl
 * @author Gianluca Merlo <gianluca.merlo@vbitalia.net>
 * @license GPLv2+
 * @since 0.1.0
 */

 namespace Wise;

 /**
  * WISE class for support functions.
  *
  * This class contains all the support functions that WISE may need.
  *
  * @package Wise\Lib
  * @author Gianluca Merlo <gianluca.merlo@vbitalia.net>
  * @since 0.1.0
  * @version 1.0.0
  */
  class Tools implements ToolsInterface {
    /**
     * Check path and decode JSON file.
     *
     * {@inheritDoc }
     *
     * @since 0.1.0
     * @access public
     * @static
     *
     * @param string $path The path of the file.
     * @param int $depth Specified recursion depth level. Default null.
     * @return bool|array False if $path is not a string, the array with the
     * content of the file otherwise.
     *
     * @throws \InvalidArgumentException if $path is not a path to a JSON file.
     * @throws \RuntimeException if the file does not exist, or on failure in
     * reading or decoding file.
     */
    public static function getJson($path, $depth = null) {
      if (! is_string($path))
        return false;

      // Check if `$path` is a path to a JSON file, and if the file exists.
      $path = trim($path);
      if (substr($path, -5) !== '.json')
        throw new \InvalidArgumentException('String passed to constructor must be a path to a JSON file');
      if (! file_exists($path))
        throw new \RuntimeException('String passed to constructor must correspond to an existing file');

      $file_content = file_get_contents($path);
      if (! $file_content)
        throw new \RuntimeException('Error in reading JSON file content');

      // Decode the file with the depth level specified.
      if ($depth == null)
        $decoded_file = json_decode($file_content, true);
      else
        $decoded_file = json_decode($file_content, true, $depth);

      if (! is_array($decoded_file))
        throw new \RuntimeException('Error in decoding JSON file');
      return $decoded_file;
    }
  }
