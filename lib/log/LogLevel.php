<?php
/**
 * LogLevel class file.
 *
 * Class of constants used to know the message type.
 *
 * Copyright (C) 2015 VB Italia Srl
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  See LICENSE.md
 *
 * @package Wise\Lib\Log
 * @copyright 2015 VB Italia Srl
 * @author Gianluca Merlo <gianluca.merlo@vbitalia.net>
 * @license GPLv2+
 * @since 0.5.0
 */

 namespace Wise\log;
 use \Psr\Log\LogLevel as PsrLogLevel;

 /**
  * Class of constants.
  *
  * This class contains all the constants corresponding to certain message
  * types.
  *
  * @package Wise\Lib\Log
  * @author Gianluca Merlo <gianluca.merlo@vbitalia.net>
  * @since 0.5.0
  * @version 1.0.0
  *
  * @codeCoverageIgnore
  */
  class LogLevel extends PsrLogLevel {
    const DEPRECATED = 'deprecated';
  }
