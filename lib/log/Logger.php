<?php
/**
 * Logger class file.
 *
 * Methods to give messages to the user.
 *
 * Copyright (C) 2015 VB Italia Srl
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  See LICENSE.md
 *
 * @package Wise\Lib\Log
 * @copyright 2015 VB Italia Srl
 * @author Gianluca Merlo <gianluca.merlo@vbitalia.net>
 * @license GPLv2+
 * @since 0.5.0
 */

namespace Wise\log;
use Wise\log\LoggerInterface;
use \Psr\Log\LoggerInterface as PsrLoggerInterface;
use \Psr\Log\AbstractLogger;

/**
 * Logger class for log messages.
 *
 * @package Wise\Lib\Log
 * @author Gianluca Merlo <gianluca.merlo@vbitalia.net>
 * @since 0.5.0
 * @version 1.2.0
 */
class Logger extends AbstractLogger implements LoggerInterface, PsrLoggerInterface {
  /**
   * Whether logging is enabled.
   *
   * @since 1.2.0
   * @access protected
   * @static
   * @var bool
   */
  protected static $enabled = true;

  /**
   * Deprecated element used.
   *
   * @since 1.1.0
   * @access public
   *
   * @param string $element Deprecated element.
   * @param string $version Version since element is deprecated.
   * @param string $replacement Optional. Element to use instead. Default null.
   */
  public function deprecated($element, $version, $replacement = null) {
    $message = '{element} is deprecated since {version}.';
    $context = array(
      'element' => $element,
      'version' => $version
    );

    if (! is_null($replacement)) {
      $message .= ' Use {replacement} instead.';
      $context['replacement'] = $replacement;
    }

    $this->log(
      LogLevel::DEPRECATED,
      $message,
      $context
    );
  }

  /**
   * Logs with an arbitrary level.
   *
   * @since 1.0.0
   * @access public
   *
   * @param mixed $level Type of log message.
   * @param string $message The message to send to the user.
   * @param array $context Other information to be placed in the message.
   * Default empty.
   *
   * @throws \InvalidArgumentException if $message is not a string.
   */
  public function log($level, $message, array $context = array()) {
    if (! is_string($message))
      throw new \InvalidArgumentException('Message passed to `log()` must be a string');

    // Return early if logging is disabled.
    // Done after argument check in order to debug errors anyway.
    if (! self::$enabled)
      return;

    $message = self::interpolate($message, $context);
    switch ($level) {
      case LogLevel::EMERGENCY:
      case LogLevel::ALERT:
      case LogLevel::CRITICAL:
      case LogLevel::ERROR:
        trigger_error($message, E_USER_ERROR);
        break;
      case LogLevel::WARNING:
        trigger_error($message, E_USER_WARNING);
        break;
      case LogLevel::NOTICE:
      case LogLevel::INFO:
      case LogLevel::DEBUG:
        trigger_error($message, E_USER_NOTICE);
        break;
      case LogLevel::DEPRECATED:
        trigger_error($message, E_USER_DEPRECATED);
        break;
    }
  }

  /**
   * Disable logging.
   *
   * @since 1.2.0
   * @access public
   * @static
   */
  public static function disable() {
    self::$enabled = false;
  }

  /**
   * Enable logging.
   *
   * @since 1.2.0
   * @access public
   * @static
   */
  public static function enable() {
    self::$enabled = true;
  }

  /**
   * Interpolate message with additional information.
   *
   * @since 1.0.0
   * @access private
   * @static
   *
   * @param string $message
   * @param array $context
   * @return string The actual message.
   */
  private static function interpolate($message, array $context = array()) {
    // Build a replacement array with braces around the context keys.
    $replace = array();
    foreach ($context as $key => $val) {
        $replace['{' . $key . '}'] = $val;
    }
    // Interpolate replacement values into the message and return.
    return strtr($message, $replace);
  }
}
