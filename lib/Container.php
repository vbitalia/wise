<?php
/**
 * Container class file.
 *
 * Copyright (C) 2015 VB Italia Srl
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  See LICENSE.md
 *
 * @package Wise\Lib
 * @copyright 2015 VB Italia Srl
 * @author Gianluca Merlo <gianluca.merlo@vbitalia.net>
 * @license GPLv2+
 * @since 0.5.0
 */

 namespace Wise;
 use Psr\Log\LoggerAwareInterface;
 use Psr\Log\LoggerInterface;

 /**
  * Amazing! This class contains things!
  *
  * @package Wise\Lib
  * @author Gianluca Merlo <gianluca.merlo@vbitalia.net>
  * @since 0.5.0
  * @version 1.0.1
  */
  class Container implements LoggerAwareInterface {
    /**
     * Instance of the singleton.
     *
     * @since 1.0.0
     * @access protected
     * @var Container
     */
    protected static $instance = null;

    /**
     * List of default services.
     *
     * @since 1.0.0
     * @access private
     * @var array
     */
    private $default_services = array(
      'logger' => 'Wise\log\Logger'
    );

    /**
     * List of services.
     *
     * @since 1.0.0
     * @access protected
     * @var array
     */
    protected $services = array();

    /**
     * Get the instance of the class.
     *
     * @since 1.0.1 Change method to be static.
     * @since 1.0.0
     * @access public
     * @static
     *
     * @return Container The instance of the class.
     */
    public static function getInstance() {
      if (is_null(self::$instance))
        self::$instance = new self;
      return self::$instance;
    }

    /**
     * Sets a Logger as a service.
     *
     * @since 1.0.0
     * @access public
     *
     * @param LoggerInterface $logger
     */
    public function setLogger(LoggerInterface $logger) {
      $this->setService('logger', $logger);
    }

    /**
     * Get the specific service of a logger.
     *
     * @since 1.0.0
     * @access public
     *
     * @return LoggerInterface The logger in the list of services.
     */
    public function getLogger() {
      return $this->getService('logger');
    }

    /**
     * Method for setting a service.
     *
     * @since 1.0.0
     * @access public
     *
     * @param string $service Identifier of the service passed.
     * @param mixed $object The object to store in the array.
     *
     * @throws \InvalidArgumentException if $service is not a string, or if
     * $object is not an object.
     */
    public function setService($service, $object) {
      if (! is_string($service))
        throw new \InvalidArgumentException('Service passed to `setService()` must be a string');
      if (! is_object($object))
        throw new \InvalidArgumentException('Object passed to `setService()` must be an object');

      $this->services[$service] = $object;
    }

    /**
     * Method for getting a service.
     *
     * @since 1.0.0
     * @access public
     *
     * @param string $service Identifier of the service needed.
     * @return mixed Object needed.
     *
     * @throws \UnexpectedValueException if $service doesn't exist in neither
     * $services and $default_services.
     */
    public function getService($service) {
      if (! array_key_exists($service, $this->services) && ! array_key_exists($service, $this->default_services))
        throw new \UnexpectedValueException(sprintf('%s is not a registered service', $service));

      if (! array_key_exists($service, $this->services))
        // If we are here, we know for sure that
        // `$this->default_services[$service]` exists.
        $this->services[$service] = new $this->default_services[$service];

      return $this->services[$service];
    }
  }
