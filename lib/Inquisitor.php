<?php
/**
 * Wise Inquisitor class file.
 *
 * Functions that WISE may need but not closely related to the algorithm.
 * The Wise Inquisitor validates arrays on behalf of Wise class.
 *
 * Copyright (C) 2015 VB Italia Srl
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  See LICENSE.md
 *
 * @package Wise\Lib
 * @copyright 2015 VB Italia Srl
 * @author Gianluca Merlo <gianluca.merlo@vbitalia.net>
 * @license GPLv2+
 * @since 0.1.0
 */

 namespace Wise;

 use Wise\Container;

 /**
  * This class is the Wise Inquisitor. Praise Him.
  *
  * This class contains all the support functions that WISE needs to check and
  * sanitize an array.
  *
  * @package Wise\Lib
  * @author Gianluca Merlo <gianluca.merlo@vbitalia.net>
  * @since 0.1.0
  * @version 1.1.3
  */
  class Inquisitor implements InquisitorInterface {
    /**
     * List of needles to search in the haystack.
     * Array of `'key' => 'value'` where 'value' is a string that will be
     * compared to the ones passed to the methods of this class.
     *
     * @since 0.1.0
     * @access protected
     * @var array
     */
    protected static $needles = array();

    /**
     * Set the `needles` property.
     *
     * Store the array passed into the `needles` property.
     *
     * @since 1.1.0
     * @access public
     * @static
     *
     * @param array $needles Associative array `"tag" => vlaue` where `value` is
     * numeric.
     */
    public static function setNeedles(array $needles) {
      self::$needles = $needles;
    }

    /**
     * Set the `needles` property to an empty array.
     *
     * @since 1.1.0
     * @access public
     * @static
     */
    public static function unsetNeedles() {
      self::$needles = array();
    }

    /**
     * Filter the array passed.
     *
     * @since 1.1.3
     * @access public
     * @static
     *
     * @param array $to_be_filtered
     * @param string $callback
     * @return array The filtered array.
     */
    public static function arrayFilter($to_be_filtered, $callback) {
      if (version_compare(PHP_VERSION, '5.6.0', '>='))
        return array_filter($to_be_filtered, array('Wise\Inquisitor', $callback), ARRAY_FILTER_USE_BOTH);

      $filtered = array();
      foreach ($to_be_filtered as $key => $value) {
        $fv = call_user_func_array(array(__CLASS__, $callback), array($value, $key));
        if ($fv == true)
          $filtered[$key] = $value;
      }
      return $filtered;
    }

    /**
     * Check consistency of `'key' => value`.
     *
     * {@inheritDoc }
     *
     * @since 0.1.0
     * @access public
     * @static
     *
     * @param string $key Key of the array to check.
     * @param number $value Value of the array to check.
     * @return bool True if $key is a string and $value is a positive non zero
     * number, false otherwise.
     */
    public static function filterPositive($value, $key) {
      return is_string($key) && is_numeric($value) && $value > 0;
    }

    /**
     * Check and filter `value`, if not in the correct form.
     *
     * {@inheritDoc }
     *
     * @since 1.1.2 Doesn't use `array_map` and `array_filter` anymore. Accept
     * a second parameter.
     * @since 1.1.1 Returns an array if `value` is partially in correct form.
     * @since 0.1.0
     * @access public
     * @static
     *
     * @param array $value Value of the array to check.
     * @param array $key Key of the array to check.
     * @return bool|array False if $value isn't in the correct form, or the
     * array with the only elements in the correct form.
     */
    public static function filterArray($value, $key) {
      if (! is_array($value))
        return false;

      $filtered_value = array();
      foreach ($value as $k => $v) {
        if ($fv = self::filterKeyNeedles($v, $k))
          $filtered_value[$k] = $fv;
      }

      $invalid = @array_diff_assoc($value, $filtered_value);
      if (! empty($invalid)) {
        $logger = self::getLogger();
        $logger->warning(
          'Invalid data found in input: {invalid}',
          array(
            'invalid' => print_r($invalid, true)
          ));

        if (empty($filtered_value))
          return false;
      }

      return $filtered_value;
    }

    /**
     * Check and filter `'key' => value`, if not in the correct form.
     *
     * {@inheritDoc }
     *
     * @since 1.1.3 No longer calls an `array_filter()`.
     * @since 1.1.1 Returns an array if `key => value` is partially in correct
     * form.
     * @since 0.1.0
     * @access public
     * @static
     *
     * @param string $key String corresponding to a modifier.
     * @param number|array $value Value to be sorted or the array with the value
     * and the priority.
     * @return bool|array False if $key and $value aren't in the correct form,
     * or an array with the only `key => value` that are in the correct form.
     */
    public static function filterKeyNeedles($value, $key) {
      // Check for key's consistency.
      if (! is_string($key) || ! in_array($key, self::$needles))
        return false;

      // Check for value's consistency.
      if (! is_numeric($value) && ! is_array($value))
        return false;

      $filtered_value = array();
      if (is_array($value)) {
        if (! array_key_exists('value', $value))
          return false;
        $filtered_value = self::arrayFilter($value, 'filterValuePriority');

        $invalid = array_diff_assoc($value, $filtered_value);
        if (! empty($invalid)) {
          self::getLogger()->warning(
            'Invalid data found in input: {invalid}',
            array(
              'invalid' => print_r($invalid, true)
            ));

          if (empty($filtered_value))
            return false;
        }
        return $filtered_value;
      }

      return $value;
    }

    /**
     * Check if `'key' => value` is in the correct form.
     *
     * {@inheritDoc }
     *
     * @since 0.4.0 Added check for `priority` with the constants in class
     * `Priority`.
     * @since 0.1.0
     * @access public
     * @static
     *
     * @param string $key The key that needs to be checked.
     * @param number|string The value that needs to be checked.
     * @return bool True if $key and $value are in the correct form,
     * false otherwise.
     */
    public static function filterValuePriority($value, $key) {
      if (! is_string($key) || ($key != 'value' && $key != 'priority'))
        return false;
      if ($key == 'value' && ! is_numeric($value))
        return false;
      if ($key == 'priority' && ! in_array($value, Priority::getPriorities())) {
        return false;
      }
      return true;
    }

    /**
     * Sanitize the array if it isn't in the correct form.
     *
     * {@inheritDoc }
     *
     * @since 1.1.2 Doesn't use `array_filter` and `array_map` anymore.
     * @since 1.1.1 Use also `array_map` to filter the array.
     * @since 0.1.0
     * @access public
     * @static
     *
     * @param array $objects Array that needs to be sanitized.
     * @param array $needles If empty, check only if values are numeric.
     * If not empty, it will be used to store the needles to be searched for in
     * the inner array's keys.
     * Default empty.
     * @return array Contains keys and values of $objects that are in the
     * correct form.
     */
    public static function sanitizeArray(array $objects, $needles = array()) {
      if (empty($objects))
        return $objects;

      if (empty($needles))
        return array_filter($objects, 'is_numeric');

      self::$needles = $needles;
      $filtered_objects = array();
      foreach ($objects as $key => $value) {
        if ($filtered_value = self::filterArray($value, $key))
          $filtered_objects[$key] = $filtered_value;
      }
      self::$needles = array();
      return $filtered_objects;
    }

    /**
     * Get the `Container` instance.
     *
     * @since 1.0.0
     * @access private
     * @static
     *
     * @return Container The instance of the Container.
     *
     * @codeCoverageIgnore
     */
    private static function getContainer() {
      return Container::getInstance();
    }

    /**
     * Get the `Logger` object saved in `Container`.
     *
     * @since 1.0.0
     * @access private
     * @static
     *
     * @return Logger
     *
     * @codeCoverageIgnore
     */
    private static function getLogger() {
      $container = self::getContainer();
      return $container->getLogger();
    }
  }
