<?php
/**
 * Priority class file.
 *
 * Class of constants used to know the value of a certain priority.
 *
 * Copyright (C) 2015 VB Italia Srl
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  See LICENSE.md
 *
 * @package Wise\Lib
 * @copyright 2015 VB Italia Srl
 * @author Gianluca Merlo <gianluca.merlo@vbitalia.net>
 * @license GPLv2+
 * @since 0.4.0
 */

 namespace Wise;

 /**
  * Class of constants.
  *
  * This class contains all the constants corresponding to certain priority
  * values.
  *
  * @package Wise\Lib
  * @author Gianluca Merlo <gianluca.merlo@vbitalia.net>
  * @since 0.4.0
  * @version 1.0.0
  */
  class Priority {
    const _DEFAULT_ = self::MEDIUM;

    const HIGH = 1;
    const MEDIUM_HIGH = 0.5;
    const MEDIUM = 0;
    const MEDIUM_LOW = -0.5;
    const LOW = -1;

    /**
     * Get the values of all the constants of this class.
     *
     * @since 1.0.0
     * @access public
     * @static
     *
     * @return array List of priority values.
     */
    public static function getPriorities() {
      return array(
        self::HIGH,
        self::MEDIUM_HIGH,
        self::MEDIUM,
        self::MEDIUM_LOW,
        self::LOW
      );
    }
  }
