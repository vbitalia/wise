<?php
/**
 * Wise Instant Sorting Engine.
 *
 * A smart sorting algorithm based on modifiers and priorities.
 *
 * Copyright (C) 2015 VB Italia Srl
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  See LICENSE.md
 *
 * @package Wise
 * @copyright 2015 VB Italia Srl
 * @author Gianluca Merlo <gianluca.merlo@vbitalia.net>
 * @license GPLv2+
 * @since 0.1.0
 */

 namespace Wise;

 /**
  * WISE interface.
  *
  * This interface defines the standards to work with the Wise class.
  *
  * @package Wise
  * @author Mattia Migliorini <mattia@vbitalia.net>
  * @since 0.3.0
  * @version 0.3.0
  */
  interface WiseInterface {
    /**
     * Class constructor.
     *
     * Initializes modifiers.
     *
     * @since 0.3.0
     * @access public
     *
     * @param array $modifiers Array of modifiers.
     */
    public function __construct(array $modifiers);

    /**
     * Order a list considering modifiers and priorities.
     *
     * This method sorts a list of objects using modifiers and priorities.
     * The priority given for a certain modifier of any element is used to raise
     * or lower the value of that specific modifier for that specific element
     * only, thus having the possibility to boost certain elements through the
     * sorting process.
     * Every `value` in the array given to the method must be an array in which
     * the keys must match with the keys in `$modifiers`. For each one of these
     * keys there can be a numeric value, or another array. This array must have
     * a key named 'value', with the previous numeric value, and an additional
     * key, called 'priority', that must be one of the constants of the class
     * `Priority`: ['HIGH', 'MEDIUM_HIGH', 'MEDIUM', 'MEDIUM_LOW', 'LOW'].
     * Default is 'MEDIUM'.
     * The returned array will have numeric ascending keys, and the values that
     * correspond to the keys of the array passed to the method.
     *
     * The returned array will have numeric ascending keys and the values that
     * correspond to the keys of the array passed to the method.
     *
     * @example https://bitbucket.org/snippets/vbitalia/98AER <3> <33>
     * How to use this method.
     *
     * @since 0.3.0
     * @access public
     *
     * @param array $objects Associative array with string keys and array-type
     * values.
     * @return array Ordered list of elements.
     */
    public function sort(array $objects);

    /**
     * Sort a simple array.
     *
     * Check if the array is in the correct form, then sort it.
     * Can be called directly if you don't care about modifiers.
     *
     * @since 0.3.0
     * @access public
     * @static
     *
     * @param array $objects Associative array with string keys and numeric
     * values.
     * @return array Ordered list of elements.
     */
    public static function simpleSort(array $objects);
  }
