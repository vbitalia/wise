<?php
/**
 * Function mocks files for Wise tests.
 *
 * Copyright (C) 2015 VB Italia Srl
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  See LICENSE.md
 *
 * @package Wise\Tests\Unit
 * @copyright 2015 VB Italia Srl
 * @author Gianluca Merlo <gianluca.merlo@vbitalia.net>
 * @since 0.6.0
 */

namespace Wise;

/**
 * Mock `array_filter()`.
 *
 * This function mocks the PHP `array_filter()` function, and returns specific
 * values useful for the tests.
 *
 * @since 0.2.0
 *
 * @param array $value The array to be filtered.
 * @return array The same array received or half the array received, or an empty
 * array.
 */
function array_filter($value) {
  $output = array();
  if(count($value) == 1 || count($value) == 5)
    $output = $value;
  if(count($value) == 3)
    $output = array();
  if(count($value) == 2) {
    array_pop($value);
    $output = $value;
  }
  if(count($value) == 6) {
    array_pop($value);
    array_pop($value);
    array_pop($value);
    $output = $value;
  }
  return $output;
}

/**
 * Mock `array_map()`.
 *
 * This function mocks the PHP `array_map()` function, and returns specific
 * values useful for the tests.
 *
 * @since 0.4.0
 *
 * @param array $value The array to be mapped.
 * @return array Contains specific keys and values.
 */
function array_map($value) {
  $output = array(
    'a' => 23,
    'b' => 42,
    'c' => 35,
    'd' => 96,
    'e' => 12,
    'f' => 36
  );

  return $output;
}
