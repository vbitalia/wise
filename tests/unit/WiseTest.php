<?php
/**
 * Unit tests for the class Wise.
 *
 * Copyright (C) 2015 VB Italia Srl
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  See LICENSE.md
 *
 * @package Wise\Tests\Unit
 * @copyright 2015 VB Italia Srl
 * @author Gianluca Merlo <gianluca.merlo@vbitalia.net>
 * @license GPLv2+
 * @since 0.2.0
 */

namespace Wise;

use \ReflectionMethod;
use \ReflectionProperty;
use PHPUnit_Framework_Assert;

 /**
  * WiseTest class.
  *
  * This class contains all the tests for the methods in class `Wise`.
  *
  * @package Wise\Tests\Unit
  * @author Gianluca Merlo <gianluca.merlo@vbitalia.net>
  * @since 0.2.0
  * @version 1.0.2
  */
  class WiseTest extends \PHPUnit_Framework_TestCase {
    /**
     * Test for method `setModifiers()`.
     *
     * This test verifies that `setModifiers()` return an array with the correct
     * values. Correct values are numbers between 0 and 10, 0 excluded.
     *
     * @since 1.0.0
     * @access public
     */
    public function testSetModifiers() {
      // Modify accessibility of the method.
      $setModifiers = new ReflectionMethod('Wise\Wise', 'setModifiers');
      $setModifiers->setAccessible(true);

      $modifiers_1 = array(
        'a' => 5,
        'b' => '10'
      );
      $modifiers_2 = array(
        'a' => 5,
        'b' => 15
      );
      $modifiers_3 = array(
        'a' => 15,
        'b' => 114
      );
      $modifiers_4 = array();

      // Calling the method passing an array already in the correct form.
      $result_1 = $setModifiers->invoke(null, $modifiers_1);
      // Calling the method passing an array with some values that aren't in the
      // correct form.
      $result_2 = $setModifiers->invoke(null, $modifiers_2);
      // Calling the method passing an array with all values not in the correct
      // form.
      $result_3 = $setModifiers->invoke(null, $modifiers_3);
      // Calling the method passing an empty array.
      $result_4 = $setModifiers->invoke(null, $modifiers_4);

      // Verify if the results are arrays.
      $this->assertTrue(is_array($result_1), 'Failed to assert true of object returned to be an array.');
      $this->assertTrue(is_array($result_2), 'Failed to assert true of object returned to be an array.');
      $this->assertTrue(is_array($result_3), 'Failed to assert true of object returned to be an array.');
      $this->assertTrue(is_array($result_4), 'Failed to assert true of object returned to be an array.');

      // Verify if the results have the same length of the arrays passed.
      $this->assertEquals(
        2,
        count($result_1),
        'Failed to assert that the object returned has the same length of the array passed.'
      );
      $this->assertEquals(
        2,
        count($result_2),
        'Failed to assert that the object returned has the same length of the array passed.'
      );
      $this->assertEquals(
        2,
        count($result_3),
        'Failed to assert that the object returned has the same length of the array passed.'
      );
      $this->assertTrue(
        empty($result_4),
        'Failed to assert that the object returned is empty.'
      );

      // Verify if the values in the returned array are the expected ones.
      $this->assertEquals(5, $result_1['a'], 'Failed to assert that `value` in returned array is equal to the expected one.');
      $this->assertEquals(10, $result_1['b'], 'Failed to assert that `value` in returned array is equal to the expected one.');
      $this->assertEquals(0.5, $result_2['a'], 'Failed to assert that `value` in returned array is equal to the expected one.');
      $this->assertEquals(1.5, $result_2['b'], 'Failed to assert that `value` in returned array is equal to the expected one.');
      $this->assertEquals(0.15, $result_3['a'], 'Failed to assert that `value` in returned array is equal to the expected one.');
      $this->assertEquals(1.14, $result_3['b'], 'Failed to assert that `value` in returned array is equal to the expected one.');
    }

    /**
     * Test for `__construct()`.
     *
     * This test verifies that `__construct()` correctly store `$modifiers` in
     * the object's property.
     *
     * @since 1.0.2 Marked as 'Incomplete'.
     * @since 1.0.0
     * @access public
     */
    public function testConstructor() {
      $this->markTestIncomplete('This test needs to be changed, according to changes in `Wise::__construct()`.');
      $modifiers_1 = array(
        'a' => 1
      );
      $modifiers_2 = array(
        'a' => 1,
        'b' => 2
      );

      // Calling `__construct()` mocking `array_filter()` to return the same
      // array.
      $wise_1 = new Wise($modifiers_1);
      $result_1 = PHPUnit_Framework_Assert::readAttribute($wise_1, 'modifiers');

      $this->assertTrue(is_array($result_1), 'Failed to assert that `$modifiers` is an array.');
      $this->assertTrue(
        count($result_1) == 1,
        'Failed to assert that `$modifiers` has the same length of the array passed to `__construct()`.'
      );
      $this->assertContains(
        1,
        $result_1,
        'Failed to assert that `$modifiers` is the same array of the one passed to `__construct()`.'
      );
      $this->assertTrue(
        array_key_exists('a', $result_1),
        'Failed to assert that `$modifiers` is the same array of the one passed to `__construct()`.'
      );

      // Calling `__construct()` mocking `array_filter()` to return half the
      // array.
      $wise_2 = new Wise($modifiers_2);
      $result_2 = PHPUnit_Framework_Assert::readAttribute($wise_2, 'modifiers');

      $this->assertTrue(is_array($result_2), 'Failed to assert that `$modifiers` is an array.');
      $this->assertTrue(
        count($result_2) == 1,
        'Failed to assert that `$modifiers` has half the length of the array passed to `__construct()`.'
      );
      $this->assertContains(
        1,
        $result_2,
        'Failed to assert that `$modifiers` has some elements of the array passed to `__construct()`.'
      );
      $this->assertNotContains(
        2,
        $result_2,
        'Failed to assert that some elements of the array passed to `__construct()` are not in `$modifiers`.'
      );
      $this->assertTrue(
        array_key_exists('a', $result_2),
        'Failed to assert that `$modifiers` has some elements of the array passed to `__construct()`.'
      );
      $this->assertFalse(
        array_key_exists('b', $result_2),
        'Failed to assert that some elements of the array passed to `__construct()` are not in `$modifiers`.'
      );
    }

    /**
     * Test for `__construct()`.
     *
     * This test verifies that `__construct()` correctly store `$modifiers` in
     * the object's property, even if it will be sanitized.
     *
     * @since 1.0.2 Marked as 'Incomplete'.
     * @since 1.0.0
     * @access public
     *
     * @expectedException PHPUnit_Framework_Error
     */
    public function testSanitizeConstructor() {
      $this->markTestIncomplete('This test needs to be changed, according to changes in `Wise::__construct()`.');
      $modifiers = array(
        'a' => 1,
        'b' => 2,
        'c' => 3
      );

      // Calling `__construct()` mocking `array_filter()` to return an empty
      // array.
      $wise = new Wise($modifiers);
      $result = PHPUnit_Framework_Assert::readAttribute( $wise, 'modifiers');

      $this->assertTrue(is_array($result), 'Failed to assert that `$modifiers` is an array.');
      $this->assertTrue(empty($result), 'Failed to assert that `$modifiers` is an empty array.');
    }

    /**
     * Test for `simpleSort()`.
     *
     * This test verifies that `simpleSort()` correctly sort the list passed.
     *
     * @since 1.0.0
     * @access public
     */
    public function testSimpleSort() {
      $correct_objects = array(
        'a' => 10,
        'b' => 45,
        'c' => 34,
        'd' => 1,
        'e' => 23
      );
      $empty_objects = array();

      // Calling the method with an array in the correct form.
      $result_1 = Wise::simpleSort($correct_objects);
      // Calling the method with an empty array.
      $result_2 = Wise::simpleSort($empty_objects);

      arsort($correct_objects);

      $this->assertTrue(
        $result_1 == array_keys($correct_objects),
        'Failed to assert that `simpleSort()` has correctly sorted the array.'
      );
      $this->assertTrue(
        empty($result_2),
        'Failed to assert that `simpleSort()` has correctly sorted an empty array.'
      );
    }

    /**
     * Test for `simpleSort()`.
     *
     * This test verifies that `simpleSort()` correctly sort the list passed,
     * even if it will be partially or totally sanitized.
     *
     * @since 1.0.0
     * @access public
     *
     * @expectedException PHPUnit_Framework_Error_Warning
     */
    public function testSanitizeSimpleSort() {
      $sanitized_objects = array(
        'a' => 45,
        'b' => 7654,
        'c' => 213,
        'd' => 'not_a_number',
        'e' => 'not_a_number',
        'f' => 'not_a_number'
      );
      $full_sanitized_objects = array(
        'a' => 'not_a_number',
        'b' => 'not_a_number',
        'c' => 'not_a_number'
      );

      // Calling the method with an array that will be sanitized.
      $result_1 = Wise::simpleSort($sanitized_objects);
      // Calling the method with an array that will be fully sanitized.
      $result_2 = Wise::simpleSort($full_sanitized_objects);

      array_pop($sanitized_objects);
      array_pop($sanitized_objects);
      array_pop($sanitized_objects);
      arsort($sanitized_objects);

      $this->assertEquals(
        3,
        count($result_1),
        'Failed to assert that `simpleSort()` has correctly sorted a sanitized array.'
      );
      $this->assertTrue(
        $result_1 == array_keys($sanitized_objects),
        'Failed to assert that `simpleSort()` has correctly sorted a sanitized array.'
      );
      $this->assertTrue(
        empty($result_2),
        'Failed to assert that `simpleSort()` has correctly sorted a fully sanitized array.'
      );
    }

    /**
     * Test for `calculate()`.
     *
     * This test verifies that `calculate()` correctly return the actual value
     * of the element passed.
     *
     * @since 1.0.0
     * @access public
     */
    public function testCalculate() {
      // Modify accessibility of the method.
      $calculate = new ReflectionMethod('Wise\Wise', 'calculate');
      $calculate->setAccessible(true);

      // Create a Wise object.
      $modifiers = array(
        'a' => 3,
        'b' => 5,
        'c' => 9,
        'd' => 9,
        'e' => 9
      );
      $wise_test = new Wise($modifiers);

      $element = array(
        'a' => 3,
        'b' => 5
      );
      $element_priority = array(
        'a' => array(
          'value' => 3,
          'priority' => 1
        ),
        'b' => array(
          'value' => 5
        )
      );
      $element_negative_priority = array(
        'a' => 3,
        'b' => array(
          'value' => 5,
          'priority' => -0.5
        )
      );

      // Calling the method passing an element without specified priorities.
      $result_1 = $calculate->invoke($wise_test, $element);
      // Calling the method passing an element with a specified priority.
      $result_2 = $calculate->invoke($wise_test, $element_priority);
      // Calling the method passing an element with a specified negative
      // priority.
      $result_3 = $calculate->invoke($wise_test, $element_negative_priority);

      $this->assertEquals(34, $result_1, 'Failed to assert that `calculate()` returns the actual value of the element passed.');
      $this->assertEquals(37, $result_2, 'Failed to assert that `calculate()` returns the actual value of the element passed.');
      $this->assertEquals(31.5, $result_3, 'Failed to assert that `calculate()` returns the actual value of the element passed.');
    }

    /**
     * Test for `sort()`.
     *
     * This test verifies that `sort()` correctly sort the list passed.
     *
     * @since 1.0.1 Marked as 'Incomplete'.
     * @since 1.0.0
     * @access public
     */
    public function testSort() {
      $this->markTestIncomplete('This test needs to be changed, according to changes in `Wise::sort()`.');
      $empty_objects = array();
      $correct_objects = array(1);

      $correct_output = array('d', 'b', 'f', 'c', 'a', 'e');

      // Create a Wise object.
      $modifiers = array('a' => 3);
      $wise_test = new Wise($modifiers);

      // Calling the method with an empty array.
      $result_1 = $wise_test->sort($empty_objects);
      // Calling the method with a correct array.
      $result_2 = $wise_test->sort($correct_objects);

      $this->assertTrue(empty($result_1), 'Failed to assert that `sort()` has correctly sorted an empty array.');
      $this->assertTrue($result_2 == $correct_output, 'Failed to assert that `sort()` has correctly sorted the array.');
    }
  }
