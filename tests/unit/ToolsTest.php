<?php
/**
 * Unit tests for the class Tools.
 *
 * Copyright (C) 2015 VB Italia Srl
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  See LICENSE.md
 *
 * @package Wise\Tests\Unit
 * @copyright 2015 VB Italia Srl
 * @author Gianluca Merlo <gianluca.merlo@vbitalia.net>
 * @license GPLv2+
 * @since 0.2.0
 */

use Wise\Tools;

 /**
  * ToolsTest class.
  *
  * This class contains all the tests for the methods in class `Tools`.
  *
  * @package Wise\Tests\Unit
  * @author Gianluca Merlo <gianluca.merlo@vbitalia.net>
  * @since 0.2.0
  * @version 1.0.0
  */
  class ToolsTest extends \PHPUnit_Framework_TestCase {
    /**
     * This method is executed once before all the tests.
     *
     * Includes the file with the method that needs to be tested.
     */
    public static function setUpBeforeClass() {
      require_once(dirname(dirname(__DIR__)).'/lib/Tools.php');
    }

    /**
     * Test for method `getJson()`.
     *
     * This test verifies that `getJson()` correctly fetch the file's content
     * with a valid path.
     *
     * @since 1.0.0
     * @access public
     */
    public function testGetJson() {
      $path = dirname(__DIR__).'/assets/json/correct_input.json';
      $decoded_array = Tools::getJson($path);
      $this->assertEquals(3, count($decoded_array), "Failed to assert that 3 is equal to the decoded array's length");
      $this->assertEquals(2, count($decoded_array["c0"]), "Failed to assert that 2 is equal to the decoded inner array's length");
    }

    /**
     * Test for method `getJson()`.
     *
     * This test verifies that `getJson()` returns false if the path given is
     * not a string.
     *
     * @since 1.0.0
     * @access public
     */
    public function testFalse() {
      $path = 42;
      $decoded_array = Tools::getJson($path);
      $this->assertFalse($decoded_array, 'Failed to assert false if the path given is not a string');
    }

    /**
     * Test for method `getJson()`.
     *
     * This test verifies that `getJson()` throws an exception if `$path` is not
     * a path to a JSON file.
     *
     * @since 1.0.0
     * @access public
     *
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage String passed to constructor must be a path to
     * a JSON file
     */
    public function testPathException() {
      $path = 'not_a_valid_path';
      Tools::getJson($path);
    }

    /**
     * Test for method `getJson()`.
     *
     * This test verifies that `getJson()` throws an exception if the file does
     * not exist.
     *
     * @since 1.0.0
     * @access public
     *
     * @expectedException \RuntimeException
     * @expectedExceptionMessage String passed to constructor must correspond to
     * an existing file
     */
    public function testFileException() {
      $path = dirname(__DIR__).'/assets/json/inexistent_input.json';
      Tools::getJson($path);
    }

    /**
     * Test for method `getJson()`.
     *
     * This test verifies that `getJson()` throws an exception if the file is
     * not in a JSON format.
     *
     * @since 1.0.0
     * @access public
     *
     * @expectedException \RuntimeException
     * @expectedExceptionMessage Error in decoding JSON file
     */
    public function testReadFileException() {
      $path = dirname(__DIR__).'/assets/json/not_a_json.json';
      Tools::getJson($path);
    }

    /**
     * Test for method `getJson()`
     *
     * This test verifies that `getJson()` throws an exception if the encoded
     * data is deeper than the recursion limit.
     *
     * @since 1.0.0
     * @access public
     *
     * @expectedException \RuntimeException
     * @expectedExceptionMessage Error in decoding JSON file
     */
    public function testRecursionLimit() {
      $path = dirname(__DIR__).'/assets/json/correct_input.json';
      Tools::getJson($path, 1);
    }
  }
