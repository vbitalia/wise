<?php
/**
 * Unit tests for the class Inquisitor.
 *
 * Copyright (C) 2015 VB Italia Srl
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  See LICENSE.md
 *
 * @package Wise\Tests\Unit
 * @copyright 2015 VB Italia Srl
 * @author Gianluca Merlo <gianluca.merlo@vbitalia.net>
 * @license GPLv2+
 * @since 0.2.0
 */

namespace Wise;

use Wise\Inquisitor;
use \ReflectionProperty;

 /**
  * InquisitorTest class.
  *
  * This class contains all the tests for the methods in class `Inquisitor`.
  *
  * @package Wise\Tests\Unit
  * @author Gianluca Merlo <gianluca.merlo@vbitalia.net>
  * @since 0.2.0
  * @version 1.0.2
  */
  class InquisitorTest extends \PHPUnit_Framework_TestCase {
    /**
     * This method is executed once before all the tests.
     *
     * Includes the file whit the methods that need to be tested.
     */
    public static function setUpBeforeClass() {
      require_once(dirname(dirname(__DIR__)).'/lib/Inquisitor.php');
    }

    /**
     * Test for method `filterPositive()`.
     *
     * This test verifies that `filterPositive()` return true if called with the
     * right parameters.
     *
     * @since 1.0.2 Marked as 'Incomplete'.
     * @since 1.0.0
     * @access public
     */
    public function testTrueFilterPositive() {
      $this->markTestIncomplete('This test needs to be changed, according to changes in `Inquisitor::filterPositive()`.');
      $key = 'a';
      $value = 5;
      $result = Inquisitor::filterPositive($value, $key);
      $this->assertTrue($result, 'Failed to assert true if `Inquisitor::filterPositive()` is called with the right parameters.');
    }

    /**
     * Test for method `filterPositive()`.
     *
     * This test verifies that `filterPositive()` return false if called with
     * the wrong parameters.
     *
     * @since 1.0.2 Marked as 'Incomplete'.
     * @since 1.0.0
     * @access public
     */
    public function testFalseFilterPositive() {
      $this->markTestIncomplete('This test needs to be changed, according to changes in `Inquisitor::filterPositive()`.');
      $string = 'a';
      $int = 5;
      $negative_int = -5;

      // Calling the method with a non string `$key`.
      $result_1 = Inquisitor::filterPositive($int, $int);
      // Calling the method with a non numeric `$value`.
      $result_3 = Inquisitor::filterPositive($string, $int);
      // Calling the method with a negative `$value`.
      $result_2 = Inquisitor::filterPositive($negative_int, $string);

      $this->assertFalse($result_1, 'Failed to assert false if `filterPositive()` is called with a non string `$key`.');
      $this->assertFalse($result_2, 'Failed to assert false if `filterPositive()` is called with a non numeric `$value`.');
      $this->assertFalse($result_3, 'Failed to assert false if `filterPositive()` is called with a negative `$value`.');
    }

    /**
     * Test for method `filterValuePriority()`.
     *
     * This test verifies that `filterValuePriority()` return true if called
     * with the right parameters.
     *
     * @since 1.0.2 Marked as 'Incomplete'.
     * @since 1.0.0
     * @access public
     */
    public function testTrueFilterPriority() {
      $this->markTestIncomplete('This test needs to be changed, according to changes in `Inquisitor::filterValuePriority()`.');
      $key_1 = 'value';
      $key_2 = 'priority';
      $value_1 = -0.5;
      $value_2 = 1;

      $result_1 = Inquisitor::filterValuePriority($value_1, $key_1);
      $result_2 = Inquisitor::filterValuePriority($value_1, $key_2);
      $result_3 = Inquisitor::filterValuePriority($value_2, $key_2);

      $this->assertTrue($result_1, 'Failed to assert true if `filterValuePriority()` is called with the right parameters.');
      $this->assertTrue($result_2, 'Failed to assert true if `filterValuePriority()` is called with the right parameters.');
      $this->assertTrue($result_3, 'Failed to assert true if `filterValuePriority()` is called with the right parameters.');
    }

    /**
     * Test for method `filterValuePriority()`.
     *
     * This test verifies that `filterValuePriority()` return false if called
     * with the wrong parameters.
     *
     * @since 1.0.2 Marked as 'Incomplete'.
     * @since 1.0.0
     * @access public
     */
    public function testFalseFilterPriority() {
      $this->markTestIncomplete('This test needs to be changed, according to changes in `Inquisitor::filterValuePriority()`.');
      $key_1 = 'value';
      $key_2 = 'priority';
      $key_3 = 'a';
      $value_1 = 3;
      $value_2 = 10;

      // Calling the method with a non string `$key`.
      $result_1 = Inquisitor::filterValuePriority($value_1, $value_1);
      // Calling the method with a `$key` != 'value' or 'priority'.
      $result_2 = Inquisitor::filterValuePriority($value_1, $key_3);
      // Calling the method with a non numeric `$value`.
      $result_3 = Inquisitor::filterValuePriority($key_3, $key_1);
      // Calling the method with a `$value` that isn't one of the accepted
      // values for 'priority'.
      $result_4 = Inquisitor::filterValuePriority($value_2, $key_2);

      $this->assertFalse($result_1, 'Failed to assert false if `filterValuePriority()` is called with a non string key.');
      $this->assertFalse(
        $result_2,
        'Failed to assert false if `filterValuePriority()` is called with non \'value\' non \'priority\' key.'
      );
      $this->assertFalse($result_3, 'Failed to assert false if `filterValuePriority()` is called with a non numeric value.');
      $this->assertFalse(
        $result_4,
        'Failed to assert false if `filterValuePriority()` is called with a not accepted value for \'priority\'.'
      );
    }

    /**
     * Test for method `filterKeyNeedles()`.
     *
     * This test verifies that `filterKeyNeedles()` return true if called with
     * the right parameters.
     *
     * @since 1.0.1 Marked as 'Incomplete'.
     * @since 1.0.0
     * @access public
     */
    public function testTrueFilterNeedles() {
      $this->markTestIncomplete('This test needs to be changed, according to changes in `Inquisitor::filterKeyNeedles()`.');
      // Mock the `needles` property of the class.
      $reflection_needles = new ReflectionProperty('Wise\Inquisitor', 'needles');
      $reflection_needles->setAccessible(true);
      $needles = array('a', 'b', 'c');
      $reflection_needles->setValue($needles);

      $key = 'b';
      $value_1 = 5;
      $value_2 = array(
        'value' => 5
      );

      // Calling the method with a numeric value.
      $result_1 = Inquisitor::filterKeyNeedles($value_1, $key);
      // Calling the method with an array value, mocking `array_filter()` to
      // return the same array given.
      $result_2 = Inquisitor::filterKeyNeedles($value_2, $key);

      $this->assertTrue($result_1, 'Failed to assert true if `filterKeyNeedles()` is called with the right parameters.');
      $this->assertTrue($result_2, 'Failed to assert true if `filterKeyNeedles()` is called with the right parameters.');
    }

    /**
     * Test for method `filterKeyNeedles()`.
     *
     * This test verifies that `filterKeyNeedles()` return false if called with
     * the wrong parameters, or true if called passing an array with some wrong
     * entries.
     *
     * @since 1.0.1 Marked as 'Incomplete'.
     * @since 1.0.0
     * @access public
     */
    public function testFalseFilterNeedles() {
      $this->markTestIncomplete('This test needs to be changed, according to changes in `Inquisitor::filterKeyNeedles()`.');
      // Mock the `needles` priority of the class.
      $reflection_needles = new ReflectionProperty('Wise\Inquisitor', 'needles');
      $reflection_needles->setAccessible(true);
      $needles = array('a', 'b', 'c');
      $reflection_needles->setValue($needles);

      $key_1 = 'not_a_needle';
      $key_2 = 'a';
      $value_1 = 5;
      $value_2 = array(
        'priority' => 3
      );

      // Calling the method with a non string `$key`.
      $result_1 = Inquisitor::filterKeyNeedles($value_1, $value_1);
      // Calling the method with a $key not in `$needles`.
      $result_2 = Inquisitor::filterKeyNeedles($value_1, $key_1);
      // Calling the method with a non numeric, non array `$value`.
      $result_3 = Inquisitor::filterKeyNeedles($key_2, $key_2);
      // Calling the method with an array `$value`, without "value" as a key.
      $result_4 = Inquisitor::filterKeyNeedles($value_2, $key_2);

      $this->assertFalse($result_1, 'Failed to assert false if `filterKeyNeedles()` is called with a non string `$key`.');
      $this->assertFalse($result_2, 'Failed to assert false if `filterKeyNeedles()` is called with a `$key` not in `$needles`.');
      $this->assertFalse($result_3, 'Failed to assert false if `filterKeyNeedles()` is called with a non numeric, non array `$value`.');
      $this->assertFalse(
        $result_4,
        'Failed to assert false if `filterKeyNeedles()` is called with an array `$value`, without \'value\' as a key.'
      );
    }

    /**
     * Test for method `filterKeyNeedles()`.
     *
     * This test verifies that `filterKeyNeedles()` return false if called with
     * the wrong parameters, or true if called passing an array with some wrong
     * entries, and that a warning is showed.
     *
     * @since 1.0.1 Marked as 'Incomplete'.
     * @since 1.0.0
     * @access public
     *
     * @expectedException PHPUnit_Framework_Error_Warning
     */
    public function testSanitizeFilterNeedles() {
      $this->markTestIncomplete('This test needs to be changed, according to changes in `Inquisitor::filterKeyNeedles()`.');
      $key = 'a';
      $value_1 = array(
        'value' => 5,
        'priority' => 2,
        'a' => 45
      );
      $value_2 = array(
        'value' => 5,
        'priority' => 2
      );
      // Calling the method with an array `$value`, mocking `array_filter()` to
      // return an empty array.
      $result_1 = Inquisitor::filterKeyNeedles($value_1, $key);
      // Calling the method with an array `$value`, mocking `array_filter()` to
      // return half the array.
      $result_2 = Inquisitor::filterKeyNeedles($value_2, $key);

      $this->assertFalse(
        $result_1,
        'Failed to assert false if `filterKeyNeedles()` is called mocking `array_filter()` to return an empty array.'
      );
      $this->assertTrue(
        $result_2,
        'Failed to assert true if `filterKeyNeedles()` is called mocking `array_filter()` to return half the array sent.'
      );
    }

    /**
     * Test for method `filterArray()`.
     *
     * This test verifies that `filterArray()` return true if called with the
     * right parameter.
     *
     * @since 1.0.1 Marked as 'Incomplete'.
     * @since 1.0.0
     * @access public
     */
    public function testTrueFilterArray() {
      $this->markTestIncomplete('This test needs to be changed, according to changes in `Inquisitor::filterArray()`.');
      $value = array(
        'a' => 5
      );

      $result = Inquisitor::filterArray($value);

      $this->assertTrue($result, 'Failed to assert true if `filterArray()` is called with the right parameter.');
    }

    /**
     * Test for method `filterArray()`.
     *
     * This test verifies that `filterArray()` return false if called with the
     * wrong parameter, or true if called passing an array with some wrong
     * entries.
     *
     * @since 1.0.1 Marked as 'Incomplete'.
     * @since 1.0.0
     * @access public
     */
    public function testFalseFilterArray() {
      $this->markTestIncomplete('This test needs to be changed, according to changes in `Inquisitor::filterArray()`.');
      $value = 5;

      // Calling the method with a non array `$value`.
      $result = Inquisitor::filterArray($value);

      $this->assertFalse($result, 'Failed to assert false if `filterArray()` is called with a non array `$value`.');
    }

    /**
     * Test for method `filterArray()`.
     *
     * This test verifies that `filterArray()` return false if called with the
     * wrong parameter, or true if called passing an array with some wrong
     * entries, and that a warning is showed.
     *
     * @since 1.0.1 Marked as 'Incomplete'.
     * @since 1.0.0
     * @access public
     *
     * @expectedException PHPUnit_Framework_Error_Warning
     */
    public function testSanitizeFilterArray() {
      $this->markTestIncomplete('This test needs to be changed, according to changes in `Inquisitor::filterArray()`.');
      $value_1 = array(
        'a' => 1,
        'b' => 2,
        'c' => 3
      );
      $value_2 = array(
        'a' => 1,
        'b' => 2
      );
      // Calling the method mocking `array_filter()` to return an empty array.
      $result_1 = Inquisitor::filterArray($value_1);
      // Calling the method mocking `array_filter()` to return half the array.
      $result_2 = Inquisitor::filterArray($value_2);

      $this->assertFalse(
        $result_1,
        'Failed to assert false if `filterArray()` is called mocking `array_filter()` to return an empty array.'
      );
      $this->assertTrue(
        $result_2,
        'Failed to assert true if `filterArray()` is called mocking `array_filter()` to return half the array sent.'
      );
    }

    /**
     * Test for method `sanitizeArray()`.
     *
     * This test verifies that `sanitizeArray()` returns an array with only the
     * `'key'=>value` pairs that are in the correct form.
     *
     * @since 1.0.1 Marked as 'Incomplete'.
     * @since 1.0.0
     * @access public
     */
    public function testSanitizeArray() {
      $this->markTestIncomplete('This test needs to be changed, according to changes in `Inquisitor::sanitizeArray()`.');
      // Mock the `needles` priority of the class.
      $reflection_needles = new ReflectionProperty('Wise\Inquisitor', 'needles');
      $reflection_needles->setAccessible(true);
      $objects_1 = array();
      $objects_2 = array(
        'a' => 1
      );
      $objects_3 = array(
        'a' => 3,
        'f' => 5
      );
      $needles = array('a', 'b', 'c');

      // Calling the method with an empty array.
      $result_1 = Inquisitor::sanitizeArray($objects_1);
      // Calling the method with no `$needles`, mocking `array_filter()` to
      // return the same array passed.
      $result_2 = Inquisitor::sanitizeArray($objects_2);
      // Calling th emethod passing the `$needles`, mocking `array_filter()` to
      // return half the array passed.
      $result_3 = Inquisitor::sanitizeArray($objects_3, $needles);

      $this->assertTrue(
        empty($result_1),
        'Failed to assert that `sanitizeArray()` returns an empty array with an empty input.'
      );
      $this->assertTrue(
        $result_2 == $objects_2,
        'Failed to assert that `sanitizeArray()` returns the same array passed if it\'s in the correct form and no needles are passed.'
      );
      $this->assertTrue(
        count($result_3) == 1,
        'Failed to assert that `sanitizeArray()` returns an array with half the length of the one passed.'
      );
      $this->assertEquals(
        $result_3['a'],
        3,
        'Failed to assert that the element in the correct form is in the array returned from `sanitizeArray()`.'
      );
      $this->assertFalse(
        array_key_exists('b', $result_3),
        'Failed to assert that the element in an incorrect form isn\'t in the array returned from `sanitizeArray()`.'
      );
    }
  }
