<?php
/**
 * Unit tests for the class WiseCommerce.
 *
 * Copyright (C) 2015 VB Italia Srl
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  See LICENSE.md
 *
 * @package Wise\Tests\Unit\Ecommerce
 * @copyright 2015 VB Italia Srl
 * @author Gianluca Merlo <gianluca.merlo@vbitalia.net>
 * @license GPLv2+
 * @since 0.6.0
 */

namespace Wise\ecommerce;

use PHPUnit_Framework_Assert;
use \ReflectionProperty;
use \ReflectionMethod;

 /**
  * WiseCommerceTest class.
  *
  * This class contains all the tests for the methods in class `WiseCommerce`.
  *
  * @package Wise\Tests\Unit\Ecommerce
  * @author Gianluca Merlo <gianluca.merlo@vbitalia.net>
  * @since 0.6.0
  * @version 0.1.1
  */
  class WiseCommerceTest extends \PHPUnit_Framework_TestCase {
    /**
     * Test for `__construct()`.
     *
     * This test verifies that `__construct()` correctly store `$user_modifiers`
     * in the object's property.
     *
     * @since 0.1.1 Marked as 'Incomplete'.
     * @since 0.1.0
     * @access public
     *
     * @requires function Wise\ecommerce\WiseCommerce::__construct
     * @requires function ReflectionProperty::setAccessible
     */
    public function testConstructorWithValidUserModifiers() {
      $this->markTestIncomplete('This test needs to be changed, according to changes in `WiseCommerce::__construct()`.');
      // Set accessibility to object's property.
      $reflection_user_modifiers = new ReflectionProperty('Wise\ecommerce\WiseCommerce', 'user_modifiers');
      $reflection_user_modifiers->setAccessible(true);

      $modifiers = array(
        'a' => 1,
        'b' => 2
      );
      $user_modifiers = array(
        'a' => 3,
        'b' => 2,
        'c' => 4
      );

      $wise_commerce = new WiseCommerce($modifiers, $user_modifiers);
      $result = $reflection_user_modifiers->getValue($wise_commerce);

      $this->assertTrue(
        $user_modifiers == $result,
        'Test failed in storing user modifiers into the object property.'
      );
    }

    /**
     * Test for `__construct()`.
     *
     * This test verifies that `__construct()` correctly set the user modifiers
     * before storing them into the object's property.
     *
     * @since 0.1.1 Marked as 'Incomplete'.
     * @since 0.1.0
     * @access public
     *
     * @requires function Wise\ecommerce\WiseCommerce::__construct
     * @requires function ReflectionProperty::setAccessible
     */
    public function testConstructorWithOutOfBoundUserModifiers() {
      $this->markTestIncomplete('This test needs to be changed, according to changes in `WiseCommerce::__construct()`.');
      // Set accessibility to object's property.
      $reflection_user_modifiers = new ReflectionProperty('Wise\ecommerce\WiseCommerce', 'user_modifiers');
      $reflection_user_modifiers->setAccessible(true);

      $modifiers = array(
        'a' => 1,
        'b' => 2
      );
      $user_modifiers = array(
        'a' => 30,
        'b' => 23,
        'c' => 4
      );
      $correct_user_modifiers = array(
        'a' => 3,
        'b' => 2.3,
        'c' => 0.4
      );

      $wise_commerce = new WiseCommerce($modifiers, $user_modifiers);
      $result = $reflection_user_modifiers->getValue($wise_commerce);

      $this->assertTrue(
        $correct_user_modifiers == $result,
        'Test failed in storing out of bound user modifiers into the object property.'
      );
    }

    /**
     * Test for `__construct()`.
     *
     * This test verifies that `__construct()` store the user modifiers into the
     * object's property, without the negative modifiers.
     *
     * @since 0.1.1 Marked as 'Incomplete'.
     * @since 0.1.0
     * @access public
     *
     * @requires function Wise\ecommerce\WiseCommerce::__construct
     */
    public function testConstructorWithSomeNegativeUserModifiers() {
      $this->markTestIncomplete('This test needs to be changed, according to changes in `WiseCommerce::__construct()`.');
      // Set accessibility to object's property.
      $reflection_user_modifiers = new ReflectionProperty('Wise\ecommerce\WiseCommerce', 'user_modifiers');
      $reflection_user_modifiers->setAccessible(true);

      $modifiers = array(
        'a' => 1,
        'b' => 2
      );
      $user_modifiers = array(
        'a' => -3,
        'b' => 2,
        'c' => 4
      );
      $correct_user_modifiers = array(
        'b' => 2,
        'c' => 4
      );

      $wise_commerce = new WiseCommerce($modifiers, $user_modifiers);
      $result = $reflection_user_modifiers->getValue($wise_commerce);

      $this->assertTrue(
        $correct_user_modifiers == $result,
        'Test failed in storing user modifiers with negative values into the object property.'
      );
    }

    /**
     * Test for `__construct()`.
     *
     * This test verifies that `__construct()` show a notice if called with no
     * valid user modifiers.
     *
     * @since 0.1.1 Marked as 'Incomplete'.
     * @since 0.1.0
     * @access public
     *
     * @expectedException PHPUnit_Framework_Error_Notice
     *
     * @requires function Wise\ecommerce\WiseCommerce::__construct
     */
    public function testConstructorWithOnlyNegativeUserModifiers() {
      $this->markTestIncomplete('This test needs to be changed, according to changes in `WiseCommerce::__construct()`.');
      $modifiers = array(
        'a' => 1,
        'b' => 2
      );
      $user_modifiers = array(
        'a' => -1,
        'b' => -2,
        'c' => -4
      );

      $wise_commerce = new WiseCommerce($modifiers, $user_modifiers);
    }

    /**
     * Test for method `sortProducts()`.
     *
     * This test verifies that `sortProducts()` correctly sorts all the products
     * passed.
     *
     * @since 0.1.0
     * @access public
     *
     * @requires function Wise\ecommerce\WiseCommerce::sortProducts
     */
    public function testSortProducts() {
      $modifiers = array(
        'a' => 1,
        'b' => 2
      );
      $user_modifiers = array(
        'a' => 3,
        'b' => 2,
        'c' => 4
      );
      $products = array('a');
      $user_products = array('b');

      $wise_commerce = new WiseCommerce($modifiers, $user_modifiers);
      $result = $wise_commerce->sortProducts($products, $user_products);
      $expected_result = array('d', 'b', 'f', 'c', 'a', 'e');

      $this->assertTrue(
        $result == $expected_result,
        'Test failed in sorting the products.'
      );
    }

    /**
     * Test for method `sortProducts()`.
     *
     * This test verifies that `sortProducts()` shows a warning if there is
     * invalid data in the arrays.
     *
     * @since 0.1.0
     * @access public
     *
     * @expectedException PHPUnit_Framework_Error_Warning
     *
     * @requires function Wise\ecommerce\WiseCommerce::sortProducts
     */
    public function testWarningSortProducts() {
      $modifiers = array(
        'a' => 1,
        'b' => 2
      );
      $user_modifiers = array(
        'a' => 3,
        'b' => 2,
        'c' => 4
      );
      $products = array('b');
      $user_products = array('b1', 'b');

      $wise_commerce = new WiseCommerce($modifiers, $user_modifiers);
      $result = $wise_commerce->sortProducts($products, $user_products);
    }

    /**
     * Test for method `sortProducts()`.
     *
     * This test verifies that `sortProducts()` returns an empty array if an
     * empty array of products is passed.
     *
     * @since 0.1.0
     * @access public
     *
     * @requires function Wise\ecommerce\WiseCommerce::sortProducts
     */
    public function testSortProductsWithEmptyProducts() {
      //TODO: mock array_map().
      $modifiers = array(
        'a' => 1,
        'b' => 2
      );
      $user_modifiers = array(
        'a' => 3,
        'b' => 2,
        'c' => 4
      );
      $products = array();
      $user_products = array('b');

      $wise_commerce = new WiseCommerce($modifiers, $user_modifiers);
      $result = $wise_commerce->sortProducts($products, $user_products);

      $this->assertTrue(
        empty($result),
        'Test failed in returning the same array if there are no products to sort.'
      );
    }

    /**
     * Test for method `sortProducts()`.
     *
     * This test verifies that `sortProducts()` correctly sort the products even
     * if no user products is passed.
     *
     * @since 0.1.0
     * @access public
     *
     * @requires function Wise\ecommerce\WiseCommerce::sortProducts
     * @requires function Wise\Wise::sort
     */
    public function testSortProductsWithEmptyUserProducts() {
      // Mock `Wise::sort()`.
      // $wise_stub = $this->getMockBuilder('Wise\Wise')
      //   ->disableOriginalConstructor()
      //   ->getMock();
      // $wise_stub->method('sort')->willReturn(array('a', 'b'));

      $modifiers = array(
        'a' => 1,
        'b' => 2,
        'c' => 1,
        'd' => 1,
        'e' => 1
      );
      $user_modifiers = array(
        'a' => 3,
        'b' => 2,
        'c' => 4
      );
      $products = array('a' => 2, 'b' => 1, 'c' => 3, 'd' => 5, 'e' => 6);
      $user_products = array();

      // $stub = $this->getMockBuilder('Wise\ecommerce\WiseCommerce')
      //   ->setConstructorArgs(array($modifiers, $user_modifiers))
      //   ->getMock();
      // $stub->method('sort')->willReturn(array('a', 'b'));
      $wise_commerce = new WiseCommerce($modifiers, $user_modifiers);
      $result = $wise_commerce->sortProducts($products, $user_products);
      // This result comes from the mocked array_map() in
      // /tests/unit/function-mock.php.
      $expected_result = array('d','b','f','c','a','e');

      $this->assertTrue(
        $result == $expected_result,
        'Test failed in sorting the products if no user products are passed.'
      );
    }

    /**
     * Test for method `setAttributes()`.
     *
     * This test verifies that `setAttributes()` correctly store the attributes
     * given in the object's property.
     *
     * @since 0.1.0
     * @access public
     *
     * @requires function Wise\ecommerce\WiseCommerce::setAttributes
     * @requires function ReflectionProperty::setAccessible
     */
    public function testSetAttributes() {
      // Set accessibility to `$attributes` property.
      $reflection_attributes = new ReflectionProperty('Wise\ecommerce\WiseCommerce', 'attributes');
      $reflection_attributes->setAccessible(true);
      // Set accessibility to the method.
      $reflection_set_attributes = new ReflectionMethod('Wise\ecommerce\WiseCommerce', 'setAttributes');
      $reflection_set_attributes->setAccessible(true);

      $attributes = array(
        'category' => 'fitness',
        'gender' => 'male'
      );
      $modifiers = array('a' => 1);
      $wise_commerce = new WiseCommerce($modifiers, $modifiers);

      $reflection_set_attributes->invoke($wise_commerce, $attributes);

      $result = $reflection_attributes->getValue($wise_commerce);
      $expected_result = array(
        'category' => array('fitness' => 1),
        'gender' => array('male' => 1)
      );

      $this->assertTrue(
        $result == $expected_result,
        'Test failed in storing the attributes in the object\'s property.'
      );
    }

    /**
     * Test for method `setAttributes()`.
     *
     * This test verifies that `setAttributes()` correctly increase the value
     * of attributes already in the object's property.
     *
     * @since 0.1.0
     * @access public
     *
     * @requires function Wise\ecommerce\WiseCommerce::setAttributes
     * @requires function ReflectionProperty::setAccessible
     */
    public function testSetAttributesIncrementAlreadyFoundAttributes() {
      // Set accessibility to `$attributes` property.
      $reflection_attributes = new ReflectionProperty('Wise\ecommerce\WiseCommerce', 'attributes');
      $reflection_attributes->setAccessible(true);
      // Set accessibility to the method.
      $reflection_set_attributes = new ReflectionMethod('Wise\ecommerce\WiseCommerce', 'setAttributes');
      $reflection_set_attributes->setAccessible(true);

      $attributes = array(
        'category' => 'fitness',
        'gender' => 'female'
      );
      $already_in_object_attributes = array(
        'category' => array('fitness' => 1),
        'gender' => array('male' => 1)
      );
      $modifiers = array('a' => 1);
      $wise_commerce = new WiseCommerce($modifiers, $modifiers);

      // Set attributes value in the object.
      $reflection_attributes->setValue($wise_commerce, $already_in_object_attributes);

      $reflection_set_attributes->invoke($wise_commerce, $attributes);

      $result = $reflection_attributes->getValue($wise_commerce);
      $expected_result = array(
        'category' => array('fitness' => 2),
        'gender' => array('male' => 1, 'female' => 1)
      );

      $this->assertTrue(
        $result == $expected_result,
        'Test failed in incrementing the attributes already in the object\'s property.'
      );
    }

    /**
     * Test for method `setAttributes()`.
     *
     * This test verifies that `setAttributes()` doesn't set any attribute if an
     * empty array is passed.
     *
     * @since 0.1.0
     * @access public
     *
     * @requires function Wise\ecommerce\WiseCommerce::setAttributes
     * @requires function ReflectionProperty::setAccessible
     */
    public function testSetAttributesWithEmptyArray() {
      // Set accessibility to `$attributes` property.
      $reflection_attributes = new ReflectionProperty('Wise\ecommerce\WiseCommerce', 'attributes');
      $reflection_attributes->setAccessible(true);
      // Set accessibility to the method.
      $reflection_set_attributes = new ReflectionMethod('Wise\ecommerce\WiseCommerce', 'setAttributes');
      $reflection_set_attributes->setAccessible(true);

      $attributes = array();
      $modifiers = array('a' => 1);
      $wise_commerce = new WiseCommerce($modifiers, $modifiers);

      $reflection_set_attributes->invoke($wise_commerce, $attributes);

      $result = $reflection_attributes->getValue($wise_commerce);

      $this->assertTrue(
        empty($result),
        'Test failed in storing an empty array of attributes in the object\'s property.'
      );
    }

    /**
     * Test for method `setAttributes()`.
     *
     * This test verifies that `setAttributes()` shows a warning if value passed
     * is an array.
     *
     * @since 0.1.0
     * @access public
     *
     * @expectedException PHPUnit_Framework_Error_Warning
     *
     * @requires function Wise\ecommerce\WiseCommerce::setAttributes
     * @requires function ReflectionProperty::setAccessible
     */
    public function testWarningSetAttributes() {
      // Set accessibility to the method.
      $reflection_set_attributes = new ReflectionMethod('Wise\ecommerce\WiseCommerce', 'setAttributes');
      $reflection_set_attributes->setAccessible(true);

      $attributes = array('category' => array());
      $modifiers = array('a' => 1);
      $wise_commerce = new WiseCommerce($modifiers, $modifiers);

      $reflection_set_attributes->invoke($wise_commerce, $attributes);
    }

    /**
     * Test for `calculateAttributes()`.
     *
     * This test verifies that `calculateAttributes()` correctly returns the
     * actual value of the attributes requested.
     *
     * @since 0.1.0
     * @access public
     *
     * @requires function Wise\ecommerce\WiseCommerce::calculateAttributes
     * @requires function ReflectionProperty::setAccessible
     * @requires function ReflectionProperty::setValue
     * @requires function ReflectionMethod::setAccessible
     */
    public function testCalculateAttributes() {
      // Set accessibility to object property.
      $reflection_attributes = new ReflectionProperty('Wise\ecommerce\WiseCommerce', 'attributes');
      $reflection_attributes->setAccessible(true);
      // Set accessibility to the method.
      $reflection_calculate_attributes = new ReflectionMethod('Wise\ecommerce\WiseCommerce', 'calculateAttributes');
      $reflection_calculate_attributes->setAccessible(true);

      $modifiers = array('a' => 1);

      // Set the object property value.
      $object_property = array(
        'category' => array('fitness' => 3, 'sauna' => 4),
        'gender' => array('male' => 2)
      );
      $wise_commerce = new WiseCommerce($modifiers, $modifiers);
      $reflection_attributes->setValue($wise_commerce, $object_property);

      $attributes = array(
        'category' => 'fitness',
        'gender' => 'male'
      );

      $result = $reflection_calculate_attributes->invoke($wise_commerce, $attributes);

      $this->assertEquals(
        $result,
        6,
        'Test failed in calculating the actual value of the attributes requested.'
      );
    }

    /**
     * Test for `calculateAttributes()`.
     *
     * This test verifies that `calculateAttributes()` correctly fetch the
     * existing attributes, and ignore the others.
     *
     * @since 0.1.0
     * @access public
     *
     * @requires function Wise\ecommerce\WiseCommerce::calculateAttributes
     * @requires function ReflectionProperty::setAccessible
     * @requires function ReflectionProperty::setValue
     * @requires function ReflectionMethod::setAccessible
     */
    public function testCalculateAttributesCalledWithNonExistingAttributes() {
      // Set accessibility to object property.
      $reflection_attributes = new ReflectionProperty('Wise\ecommerce\WiseCommerce', 'attributes');
      $reflection_attributes->setAccessible(true);
      // Set accessibility to the method.
      $reflection_calculate_attributes = new ReflectionMethod('Wise\ecommerce\WiseCommerce', 'calculateAttributes');
      $reflection_calculate_attributes->setAccessible(true);

      $modifiers = array('a' => 1);

      // Set the object property value.
      $object_property = array(
        'category' => array('fitness' => 3, 'sauna' => 4),
        'gender' => array('male' => 2)
      );
      $wise_commerce = new WiseCommerce($modifiers, $modifiers);
      $reflection_attributes->setValue($wise_commerce, $object_property);

      $attributes = array(
        'category' => 'fitness',
        'gender' => 'female'
      );

      $result = $reflection_calculate_attributes->invoke($wise_commerce, $attributes);

      $this->assertEquals(
        $result,
        3,
        'Test failed in calculating the actual value of the existing attributes requested.'
      );
    }

    /**
     * Test for `calculateAttributes()`.
     *
     * This test verifies that `calculateAttributes()` correctly returns 1 if
     * called with an empty array.
     *
     * @since 0.1.0
     * @access public
     *
     * @requires function Wise\ecommerce\WiseCommerce::calculateAttributes
     * @requires function ReflectionProperty::setAccessible
     * @requires function ReflectionProperty::setValue
     */
    public function testCalculateAttributesWithoutPassingThem() {
      // Set accessibility to object property.
      $reflection_attributes = new ReflectionProperty('Wise\ecommerce\WiseCommerce', 'attributes');
      $reflection_attributes->setAccessible(true);
      // Set accessibility to the method.
      $reflection_calculate_attributes = new ReflectionMethod('Wise\ecommerce\WiseCommerce', 'calculateAttributes');
      $reflection_calculate_attributes->setAccessible(true);

      $modifiers = array('a' => 1);

      // Set the object property value.
      $object_property = array(
        'category' => array('fitness' => 3, 'sauna' => 4),
        'gender' => array('male' => 2)
      );
      $wise_commerce = new WiseCommerce($modifiers, $modifiers);
      $reflection_attributes->setValue($wise_commerce, $object_property);

      $attributes = array();

      $result = $reflection_calculate_attributes->invoke($wise_commerce, $attributes);

      $this->assertEquals(
        $result,
        1,
        'Test failed in calling `calculateAttributes()` with an empty array.'
      );
    }

    /**
     * Test for `calculateAttributes()`.
     *
     * This test verifies that `calculateAttributes()` correctly returns 1 if
     * attributes property is empty.
     *
     * @since 0.1.0
     * @access public
     *
     * @requires function Wise\ecommerce\WiseCommerce::calculateAttributes
     * @requires function ReflectionProperty::setAccessible
     */
    public function testCalculateAttributesWithEmptyObjectProperty() {
      // Set accessibility to object property.
      $reflection_attributes = new ReflectionProperty('Wise\ecommerce\WiseCommerce', 'attributes');
      $reflection_attributes->setAccessible(true);
      // Set accessibility to the method.
      $reflection_calculate_attributes = new ReflectionMethod('Wise\ecommerce\WiseCommerce', 'calculateAttributes');
      $reflection_calculate_attributes->setAccessible(true);

      $modifiers = array('a' => 1);
      $wise_commerce = new WiseCommerce($modifiers, $modifiers);

      $attributes = array(
        'category' => 'fitness',
        'gender' => 'male'
      );

      $result = $reflection_calculate_attributes->invoke($wise_commerce, $attributes);

      $this->assertEquals(
        $result,
        1,
        'Test failed in returning 1 if no attributes are found in the objects\'s property.'
      );
    }

    /**
     * Test for method `calculateUser()`.
     *
     * This test verifies that `calculateUser()` correctly calculates the actual
     * value of a product.
     *
     * @since 0.1.1 Marked as 'Incomplete'.
     * @since 0.1.0
     * @access public
     *
     * @requires Wise\ecommerce\WiseCommerce::calculateUser
     * @requires function ReflectionMethod::setAccessible
     */
    public function testCalculateUser() {
      $this->markTestIncomplete('This test needs to be changed, according to changes in `WiseCommerce::calculateUser()`.');
      // Set accessibility of the method.
      $reflection_calculate_user = new ReflectionMethod('Wise\ecommerce\WiseCommerce', 'calculateUser');
      $reflection_calculate_user->setAccessible(true);
      // Set accessibility to `attributes` property.
      $reflection_attributes = new ReflectionProperty('Wise\ecommerce\WiseCommerce', 'attributes');
      $reflection_attributes->setAccessible(true);

      $modifiers = array(
        'a' => 1,
        'b' => 2,
        'c' => 3,
        'd' => 4,
        'e' => 5
      );
      $wise_commerce = new WiseCommerce($modifiers, $modifiers);

      $user_product = array(
        'a' => 34,
        'b' => 12,
        'category' => 'fitness'
      );

      $result = $reflection_calculate_user->invoke($wise_commerce, $user_product);

      $this->assertEquals(
        $result,
        5.8,
        'Test failed in calculating the actual value of a user product.'
      );

      $attr = $reflection_attributes->getValue($wise_commerce);
      $expected_attr = array('category' => array('fitness' => 1));
      $this->assertTrue(
        $attr == $expected_attr,
        'Test failed in dividing up the array passed.'
      );
    }

    /**
     * Test for method `calculateUser()`.
     *
     * This test verifies that `calculateUser()` returns 0 if the product hasn't
     * modifiers in it.
     *
     * @since 0.1.1 Marked as 'Incomplete'.
     * @since 0.1.0
     * @access public
     *
     * @requires Wise\ecommerce\WiseCommerce::calculateUser
     * @requires function ReflectionMethod::setAccessible
     */
    public function testCalculateUserWithNoModifiersInProduct() {
      $this->markTestIncomplete('This test needs to be changed, according to changes in `WiseCommerce::calculateUser()`.');
      // Set accessibility of the method.
      $reflection_calculate_user = new ReflectionMethod('Wise\ecommerce\WiseCommerce', 'calculateUser');
      $reflection_calculate_user->setAccessible(true);

      $modifiers = array(
        'a' => 1,
        'b' => 2
      );
      $wise_commerce = new WiseCommerce($modifiers, $modifiers);

      $user_product = array(
        'category' => 'fitness'
      );

      $result = $reflection_calculate_user->invoke($wise_commerce, $user_product);

      $this->assertEquals(
        $result,
        0,
        'Test failed in calculating the actual value of a user product without modifiers.'
      );
    }

    /**
     * Test for method `calculateUser()`.
     *
     * This test verifies that `calculateUser()` returns the actual value of the
     * correct data if some data are in an incorrect form.
     *
     * @since 0.1.1 Marked as 'Incomplete'.
     * @since 0.1.0
     * @access public
     *
     * @requires Wise\ecommerce\WiseCommerce::calculateUser
     * @requires function ReflectionMethod::setAccessible
     */
    public function testCalculateUserWithSomeDataInIncorrectForm() {
      $this->markTestIncomplete('This test needs to be changed, according to changes in `WiseCommerce::calculateUser()`.');
      // Set accessibility of the method.
      $reflection_calculate_user = new ReflectionMethod('Wise\ecommerce\WiseCommerce', 'calculateUser');
      $reflection_calculate_user->setAccessible(true);

      $modifiers = array(
        'a' => 1,
        'b' => 2
      );
      $wise_commerce = new WiseCommerce($modifiers, $modifiers);

      $user_product = array(
        'c' => 34,
        'a' => 12,
        'category' => 'fitness'
      );

      $result = $reflection_calculate_user->invoke($wise_commerce, $user_product);

      $this->assertEquals(
        $result,
        1.2,
        'Test failed in calculating the actual value of a user product without modifiers.'
      );
    }

    /**
     * Test for method `calculateProduct()`.
     *
     * This test verifies that `calculateProduct()` correctly returns the actual value
     * of the product.
     *
     * @since 0.1.1 Marked as 'Incomplete'.
     * @since 0.1.0
     * @access public
     *
     * @requires function Wise\ecommerce\WiseCommerce::calculateProduct
     * @requires function ReflectionMethod::setAccessible
     */
    public function testCalculateProduct() {
      $this->markTestIncomplete('This test needs to be changed, according to changes in `WiseCommerce::calculateProduct()`.');
      // Set accessibility to the method
      $reflection_calculate = new ReflectionMethod('Wise\ecommerce\WiseCommerce', 'calculateProduct');
      $reflection_calculate->setAccessible(true);
      // Set accessibility to `attributes` property.
      $reflection_attributes = new ReflectionProperty('Wise\ecommerce\WiseCommerce', 'attributes');
      $reflection_attributes->setAccessible(true);

      $modifiers = array('a' => 2);
      $wise_commerce = new WiseCommerce($modifiers, $modifiers);

      // Set a value to `attributes` property.
      $attr = array('category' => array('fitness' => 5));
      $reflection_attributes->setValue($wise_commerce, $attr);

      $product = array('a' => 250, 'category' => 'fitness');
      $result = $reflection_calculate->invoke($wise_commerce, 'product', $product);

      $this->assertEquals(
        $result,
        2500,
        'Test failed in calculating the actual value of a product.'
      );
    }

    /**
     * Test for method `calculateProduct()`.
     *
     * This test verifies that `calculateProduct()` correctly returns the actual value
     * of the product if the product is also in the `user_prods` object's
     * property.
     *
     * @since 0.1.1 Marked as 'Incomplete'.
     * @since 0.1.0
     * @access public
     *
     * @requires function Wise\ecommerce\WiseCommerce::calculateProduct
     * @requires function ReflectionMethod::setAccessible
     * @requires function ReflectionProperty::setAccessible
     */
    public function testCalculateWithProductInProperty() {
      $this->markTestIncomplete('This test needs to be changed, according to changes in `WiseCommerce::calculateProduct()`.');
      // Set accessibility to the method
      $reflection_calculate = new ReflectionMethod('Wise\ecommerce\WiseCommerce', 'calculateProduct');
      $reflection_calculate->setAccessible(true);
      // Set accessibility to `attributes` property.
      $reflection_attributes = new ReflectionProperty('Wise\ecommerce\WiseCommerce', 'attributes');
      $reflection_attributes->setAccessible(true);
      // Set accessibility to `user_prods` property.
      $reflection_user_prods = new ReflectionProperty('Wise\ecommerce\WiseCommerce', 'user_prods');
      $reflection_user_prods->setAccessible(true);

      $modifiers = array('a' => 2);
      $wise_commerce = new WiseCommerce($modifiers, $modifiers);

      // Set a value to `attributes` property.
      $attr = array('category' => array('fitness' => 5));
      $reflection_attributes->setValue($wise_commerce, $attr);

      // Set a value to `user_prods` property.
      $usr_prd = array('product' => 10);
      $reflection_user_prods->setValue($wise_commerce, $usr_prd);

      $product = array('a' => 250, 'category' => 'fitness');
      $result = $reflection_calculate->invoke($wise_commerce, 'product', $product);

      $this->assertEquals(
        $result,
        25000,
        'Test failed in calculating the actual value of a product.'
      );
    }

    /**
     * Test for method `calculateProduct()`.
     *
     * This test verifies that `calculateProduct()` returns 0 if `$product` isn't an
     * array.
     *
     * @since 0.1.1 Marked as 'Incomplete'.
     * @since 0.1.0
     * @access public
     *
     * @requires function Wise\ecommerce\WiseCommerce::calculateProduct
     * @requires function ReflectionMethod::setAccessible
     */
    public function testCalculateReturnsZero() {
      $this->markTestIncomplete('This test needs to be changed, according to changes in `WiseCommerce::calculateProduct()`.');
      // Set accessibility to the method
      $reflection_calculate = new ReflectionMethod('Wise\ecommerce\WiseCommerce', 'calculateProduct');
      $reflection_calculate->setAccessible(true);

      $modifiers = array('a' => 1);
      $wise_commerce = new WiseCommerce($modifiers, $modifiers);

      $product = 12;
      $result = $reflection_calculate->invoke($wise_commerce, 'product', $product);

      $this->assertEquals(
        $result,
        0,
        'Test failed in calculating the actual value of a product if `parent::calculate()` returns 0.'
      );
    }
  }
