<?php
/**
 * Function mocks files for WiseCommerce tests.
 *
 * Copyright (C) 2015 VB Italia Srl
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  See LICENSE.md
 *
 * @package Wise\Tests\Unit
 * @copyright 2015 VB Italia Srl
 * @author Gianluca Merlo <gianluca.merlo@vbitalia.net>
 * @since 0.6.0
 */

namespace Wise\ecommerce;

/**
 * Mock `array_filter()`.
 *
 * This function mocks the PHP `array_filter()` function, and returns specific
 * values useful for the tests.
 *
 * @since 0.6.0
 *
 * @param array $value The array to be filtered.
 * @return array Array with values useful to the tests.
 */
function array_filter($value) {
  if (in_array('b1', $value))
    $object = array('b');
  elseif (array_key_exists('a', $value) && $value['a'] == -3)
    $object = array(
      'b' => 2,
      'c' => 4
    );
  elseif (array_key_exists('a', $value) && $value['a'] == -1)
    $object = array();
  else
    $object = $value;
  return $object;
}

/**
 * Mock `array_map()`.
 *
 * This function mocks the PHP `array_map()` function, and returns specific
 * values useful for the tests.
 *
 * @since 0.6.0
 *
 * @param array $value The array to be mapped.
 * @return array Contains specific keys and values.
 */
function array_map($useless, $value, $other_value = 0) {
  if ($other_value == 0)
    switch ($value[0]) {
      case 'b':
        $output = array(
          'a' => 23
        );
        break;
      case 'a':
        $output = array(
          'a' => 23,
          'b' => 42,
          'c' => 35,
          'd' => 96,
          'e' => 12,
          'f' => 36
        );
        break;
    }
  else
    switch ($other_value[0]) {
      case 'b':
        $output = array(
          'a' => 23
        );
        break;
      case 'a':
        $output = array(
          'a' => 23,
          'b' => 42,
          'c' => 35,
          'd' => 96,
          'e' => 12,
          'f' => 36
        );
        break;
    }

  return $output;
}
