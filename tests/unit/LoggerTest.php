<?php
/**
 * Unit tests for the class `Logger`.
 *
 * Copyright (C) 2015 VB Italia Srl
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  See LICENSE.md
 *
 * @package Wise\Tests\Unit
 * @copyright 2015 VB Italia Srl
 * @author Gianluca Merlo <gianluca.merlo@vbitalia.net>
 * @license GPLv2+
 * @since 0.5.0
 */

namespace Wise;

use Wise\log\Logger;
use Wise\log\LogLevel;
use \ReflectionMethod;
use \ReflectionProperty;

/**
 * LoggerTest class.
 *
 * This class contains all the tests for the methods in class `Logger`.
 *
 * @package Wise\Tests\Unit
 * @author Gianluca Merlo <gianluca.merlo@vbitalia.net>
 * @author Mattia Migliorini <mattia@vbitalia.net>
 * @since 0.5.0
 * @version 1.2.0
 */
class LoggerTest extends \PHPUnit_Framework_TestCase {
  /**
   * Test for method `interpolate()`.
   *
   * This test verifies that `interpolate()` correctly returns the string with
   * the identifiers replaced with the correct values.
   *
   * @since 1.0.0
   * @access public
   */
  public function testInterpolate() {
    // Set accessibility to the method
    $interpolate = new ReflectionMethod('Wise\log\Logger', 'interpolate');
    $interpolate->setAccessible(true);

    $message_1 = 'Invalid data found in argument passed to the method.';
    $message_2 = 'Invalid data found in {data}.';
    $context = array(
      'data' => 'argument passed to the method'
    );

    // Calling the method with an empty `context`.
    $result_1 = $interpolate->invoke(null, $message_1);
    // Calling the method with a non empty `context`.
    $result_2 = $interpolate->invoke(null, $message_2, $context);

    $this->assertEquals($result_1, $message_1, 'Test failed in interpolate a `message` with an empty array.');
    $this->assertEquals($result_2, $message_1, 'Test failed in interpolate a `message` with a non empty array.');
  }

  /**
   * Test for method `log()`.
   *
   * This test verifies that `log()` throws an exception if `$message` is not
   * a string
   *
   * @since 1.0.0
   * @access public
   *
   * @expectedException InvalidArgumentException
   */
  public function testExceptionLog() {
    $logger = new Logger();
    $message = 42;

    $logger->log(LogLevel::ERROR, $message);
  }

  /**
   * Test for method `log()`.
   *
   * This test verifies that `log()` correctly shows an error when called with
   * `$level = LogLevel::EMERGENCY`.
   *
   * @since 1.0.0
   * @access public
   *
   * @expectedException PHPUnit_Framework_Error
   */
  public function testEmergencyLog() {
   $logger = new Logger();
    $message = 'This is a message.';

    $logger->log(LogLevel::EMERGENCY, $message);
  }

  /**
   * Test for method `log()`.
   *
   * This test verifies that `log()` correctly shows an error when called with
   * `$level = LogLevel::ALERT`.
   *
   * @since 1.0.0
   * @access public
   *
   * @expectedException PHPUnit_Framework_Error
   */
  public function testAlertLog() {
    $logger = new Logger();
    $message = 'This is a message.';

    $logger->log(LogLevel::ALERT, $message);
  }

  /**
   * Test for method `log()`.
   *
   * This test verifies that `log()` correctly shows an error when called with
   * `$level = LogLevel::CRITICAL`.
   *
   * @since 1.0.0
   * @access public
   *
   * @expectedException PHPUnit_Framework_Error
   */
  public function testCriticalLog() {
    $logger = new Logger();
    $message = 'This is a message.';

    $logger->log(LogLevel::CRITICAL, $message);
  }

  /**
   * Test for method `log()`.
   *
   * This test verifies that `log()` correctly shows an error when called with
   * `$level = LogLevel::ERROR`.
   *
   * @since 1.0.0
   * @access public
   *
   * @expectedException PHPUnit_Framework_Error
   */
  public function testErrorLog() {
    $logger = new Logger();
    $message = 'This is a message.';

    $logger->log(LogLevel::ERROR, $message);
  }

  /**
   * Test for method `log()`.
   *
   * This test verifies that `log()` correctly shows a warning when called
   * with `$level = LogLevel::WARNING`.
   *
   * @since 1.0.0
   * @access public
   *
   * @expectedException PHPUnit_Framework_Error_Warning
   */
  public function testWarningLog() {
    $logger = new Logger();
    $message = 'This is a message.';

    $logger->log(LogLevel::WARNING, $message);
  }

  /**
   * Test for method `log()`.
   *
   * This test verifies that `log()` correctly shows a notice when called with
   * `$level = LogLevel::NOTICE`.
   *
   * @since 1.0.0
   * @access public
   *
   * @expectedException PHPUnit_Framework_Error_Notice
   */
  public function tesNoticeLog() {
    $logger = new Logger();
    $message = 'This is a message.';

    $logger->log(LogLevel::NOTICE, $message);
  }

  /**
   * Test for method `log()`.
   *
   * This test verifies that `log()` correctly shows a notice when called with
   * `$level = LogLevel::INFO`.
   *
   * @since 1.0.0
   * @access public
   *
   * @expectedException PHPUnit_Framework_Error_Notice
   */
  public function testInfoLog() {
    $logger = new Logger();
    $message = 'This is a message.';

    $logger->log(LogLevel::INFO, $message);
  }

  /**
   * Test for method `log()`.
   *
   * This test verifies that `log()` correctly shows a notice when called with
   * `$level = LogLevel::DEBUG`.
   *
   * @since 1.0.0
   * @access public
   *
   * @expectedException PHPUnit_Framework_Error_Notice
   */
  public function testDebugLog() {
    $logger = new Logger();
    $message = 'This is a message.';

    $logger->log(LogLevel::DEBUG, $message);
  }

  /**
   * Test for method `log()`.
   *
   * This test verifies that `log()` correctly shows an error when called with
   * `$level = LogLevel::DEPRECATED`.
   *
   * @since 1.0.0
   * @access public
   *
   * @expectedException PHPUnit_Framework_Error
   */
  public function testDeprecatedLog() {
    $logger = new Logger();
    $message = 'This is a message.';

    $logger->log(LogLevel::DEPRECATED, $message);
  }

  /**
   * Test for method `deprecated()` without replacement.
   *
   * This test verifies that `Logger::deprecated()` passes the correct parameters
   * to `Logger::log()` when called without a replacement.
   *
   * @since 1.1.0
   * @access public
   *
   * @author Mattia Migliorini <mattia@vbitalia.net>
   *
   * @group deprecated
   *
   * @covers Wise\log\Logger::deprecated
   */
  public function testPassDeprecatedMessageAndContextWithoutReplacement() {
    // Setup test arguments.
    $function = 'function';
    $version = '1.0.0';

    $this->expectOutputString(LogLevel::DEPRECATED.".{element} is deprecated since {version}.k:element=>v:$function;k:version=>v:$version;");

    // Create a stub for the Logger class.
    $logger = $this->getMockBuilder('Wise\log\Logger')
      ->setMethods(array('log'))
      ->getMock();

    // Configure the stub.
    // Mock only the log() method.
    $logger->method('log')->will($this->returnCallback(function($level, $message, $context) {
      echo $level.'.'.$message;
      foreach ($context as $k => $v)
        echo "k:$k=>v:$v;";
    }));

    $logger->deprecated(
      $function,
      $version
    );
  }

  /**
   * Test for method `deprecated()` with replacement.
   *
   * This test verifies that `Logger::deprecated()` passes the correct parameters
   * to `Logger::log()` when called with replacement.
   *
   * @since 1.1.0
   * @access public
   *
   * @author Mattia Migliorini <mattia@vbitalia.net>
   *
   * @group deprecated
   *
   * @covers Wise\log\Logger::deprecated
   */
  public function testPassDeprecatedMessageAndContextWithReplacement() {
    //  Setup arguments.
    $function = 'function';
    $version = '1.0.0';
    $replacement = 'replaced';

    $this->expectOutputString(LogLevel::DEPRECATED.".{element} is deprecated since {version}. Use {replacement} instead.k:element=>v:$function;k:version=>v:$version;k:replacement=>v:$replacement;");

    // Create a stub for the Logger class.
    $logger = $this->getMockBuilder('Wise\log\Logger')
      ->setMethods(array('log'))
      ->getMock();

    $logger->method('log')->will($this->returnCallback(function($level, $message, $context) {
      echo $level.'.'.$message;
      foreach ($context as $k => $v)
        echo "k:$k=>v:$v;";
    }));

    $logger->deprecated(
      $function,
      $version,
      $replacement
    );
  }

  /**
   * Test that `Logger::$enabled` is true by default.
   *
   * @since 1.2.0
   * @access public
   *
   * @requires function ReflectionProperty::setAccessible
   * @requires function ReflectionProperty::getValue
   */
  public function testLoggingEnabledByDefault() {
    $enabled = new ReflectionProperty('Wise\log\Logger', 'enabled');
    $enabled->setAccessible(true);

    $logger = new Logger;
    $this->assertTrue(
      $enabled->getValue($logger),
      'Logger::$enabled is not true by default.'
    );
  }

  /**
   * Test that `Logger::$enabled` is false after logging has been disabled.
   *
   * @since 1.2.0
   * @access public
   *
   * @requires function ReflectionProperty::setAccessible
   * @requires function ReflectionProperty::getValue
   */
  public function testDisableLogging() {
    $enabled = new ReflectionProperty('Wise\log\Logger', 'enabled');
    $enabled->setAccessible(true);

    $logger = new Logger;

    $this->assertTrue(
      $enabled->getValue($logger),
      'Logger::$enabled is not true by default.'
    );

    $logger->disable();

    $this->assertFalse(
      $enabled->getValue($logger),
      'Logger::$enabled is not false after logging has been disabled.'
    );

    // Reset enabled state.
    $logger->enable();
  }

  /**
   * Test that `Logger::$enabled` is true after logging has been enabled.
   *
   * @since 1.2.0
   * @access public
   *
   * @requires function ReflectionProperty::setAccessible
   * @requires function ReflectionProperty::getValue
   */
  public function testEnableLogging() {
    $enabled = new ReflectionProperty('Wise\log\Logger', 'enabled');
    $enabled->setAccessible(true);

    $logger = new Logger;

    $enabled->setValue(false);

    $logger->enable();

    $this->assertTrue(
      $enabled->getValue($logger),
      'Logger::$enabled is not true after logging has been enabled.'
    );
  }
}
