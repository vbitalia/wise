<?php
/**
 * Unit tests for the class `Priority`.
 *
 * Copyright (C) 2015 VB Italia Srl
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  See LICENSE.md
 *
 * @package Wise\Tests\Unit
 * @copyright 2015 VB Italia Srl
 * @author Gianluca Merlo <gianluca.merlo@vbitalia.net>
 * @license GPLv2+
 * @since 0.5.0
 */

namespace Wise;

use Wise\Priority;

 /**
  * PriorityTest class.
  *
  * This class contains all the tests for the methods in class `Priority`.
  *
  * @package Wise\Tests\Unit
  * @author Gianluca Merlo <gianluca.merlo@vbitalia.net>
  * @since 0.5.0
  * @version 1.0.0
  */
  class PriorityTest extends \PHPUnit_Framework_TestCase {
    /**
     * Test for method `getPriorities()`.
     *
     * This test verifies that `getPriorities()` correctly returns an array with
     * the right priority values.
     *
     * @since 1.0.0
     * @access public
     */
    public function testGetPriorities() {
      $result = Priority::getPriorities();

      $this->assertEquals(
        5,
        count($result),
        'Test failed in fetching the right number of priority values.'
      );
      $this->assertTrue(
        in_array(Priority::HIGH, $result),
        'Test failed in fetching `HIGH` priority value.'
      );
      $this->assertTrue(
        in_array(Priority::MEDIUM_HIGH, $result),
        'Test failed in fetching `MEDIUM_HIGH` priority value.'
      );
      $this->assertTrue(
        in_array(Priority::MEDIUM, $result),
        'Test failed in fetching `MEDIUM` priority value.'
      );
      $this->assertTrue(
        in_array(Priority::MEDIUM_LOW, $result),
        'Test failed in fetching `MEDIUM_LOW` priority value.'
      );
      $this->assertTrue(
        in_array(Priority::LOW, $result),
        'Test failed in fetching `LOW` priority value.'
      );
    }
  }
