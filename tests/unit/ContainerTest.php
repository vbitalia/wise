<?php
/**
 * Unit tests for the class `Container`.
 *
 * Copyright (C) 2015 VB Italia Srl
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  See LICENSE.md
 *
 * @package Wise\Tests\Unit
 * @copyright 2015 VB Italia Srl
 * @author Gianluca Merlo <gianluca.merlo@vbitalia.net>
 * @license GPLv2+
 * @since 0.5.0
 */

namespace Wise;

use Wise\Container;
use Wise\log\Logger;
use \ReflectionProperty;

 /**
  * ContainerTest class.
  *
  * This class contains all the tests for the methods in class `Container`.
  *
  * @package Wise\Tests\Unit
  * @author Gianluca Merlo <gianluca.merlo@vbitalia.net>
  * @since 0.5.0
  * @version 1.0.0
  */
  class ContainerTest extends \PHPUnit_Framework_TestCase {
    /**
     * Test for method `getInstance()`.
     *
     * This test verifies that `getInstance()` correctly return an instance of
     * `Container`.
     *
     * @since 1.0.0
     * @access public
     */
    public function testGetInstance() {
      $services = array(
        'logger' => 12
      );
      $container = new Container();
      $container_instance = $container->getInstance();
      $this->assertTrue(
        $container_instance instanceof Container,
        'Failed to test that `Container::$instance` has been created.'
      );
      $container_instance = $container->getInstance();
      $this->assertTrue(
        $container_instance instanceof Container,
        'Failed to test that `Container::$instance` was already created.'
      );
    }

    /**
     * Test for method `setService()`.
     *
     * This test verifies that `setService()` correctly throws an exception if
     * the first argument passed is not a string.
     *
     * @since 1.0.0
     * @access public
     *
     * @expectedException InvalidArgumentException
     * @expectedExceptionMessage Service passed to `setService()` must be a
     * string
     */
    public function testNotStringSetService() {
      $container = new Container();
      $container->setService(42, new Logger());
    }

    /**
     * Test for method `setService()`.
     *
     * This test verifies that `setService()` correctly throws an exception if
     * the second argument passed is not an object.
     *
     * @since 1.0.0
     * @access public
     *
     * @expectedException InvalidArgumentException
     * @expectedExceptionMessage Object passed to `setService()` must be an
     * object
     */
    public function testNotObjectSetService() {
      $container = new Container();
      $container->setService('logger', 42);
    }

    /**
     * Test for method `setService()`.
     *
     * This test verifies that `setService()` correctly put a service into
     * services list.
     *
     * @since 1.0.0
     * @access public
     *
     * @requires function ReflectionProperty::setAccessible
     */
    public function testSetService() {
        // Set accessibility to `$services` property.
        $reflection_services = new ReflectionProperty('Wise\Container', 'services');
        $reflection_services->setAccessible(true);

        $container = new Container();
        $container->setService('logger', new Logger());
        $services = $reflection_services->getValue($container);

        $this->assertTrue(
          array_key_exists('logger', $services),
          'Failed to test that setService correctly puts the `Logger` passed into the services list.'
        );
        $this->assertTrue(
          $services['logger'] instanceof Logger,
          'Failed to test that setService correctly puts the `Logger` passed into the services list.'
        );
    }

    /**
     * Test for method `setLogger()`.
     *
     * This test verifies that `setLogger()` correctly put a `Logger` into
     * services list.
     *
     * @since 1.0.0
     * @access public
     *
     * @requires function ReflectionProperty::setAccessible
     */
    public function testSetLogger() {
      // Set accessibility to `$services` property.
      $reflection_services = new ReflectionProperty('Wise\Container', 'services');
      $reflection_services->setAccessible(true);

      $container = new Container();
      $logger = new Logger();
      $container->setLogger($logger);
      $services = $reflection_services->getValue($container);

      $this->assertTrue(
        array_key_exists('logger', $services),
        'Failed to test that setLogger correctly puts the `Logger` passed into the services list.'
      );
      $this->assertTrue(
        $services['logger'] instanceof Logger,
        'Failed to test that setLogger correctly puts the `Logger` passed into the services list.'
      );
    }

    /**
     * Test for `getService()`.
     *
     * This test verifies that `getService()` correctly throws an exception if
     * the identifier of the service searched does't exist in the services list
     * nor in the default services list.
     *
     * @since 1.0.0
     * @access public
     *
     * @requires function ReflectionProperty::setAccessible
     *
     * @expectedException UnexpectedValueException
     */
    public function testExceptionGetService() {
      $container = new Container();
      $container->getService('not_in_service_list');
    }

    /**
     * Test for `getService()`.
     *
     * This test verifies that `getService()` correctly returns the service
     * searched.
     *
     * @since 1.0.0
     * @access public
     */
    public function testGetService() {
      // Set accessibility to `$services` property.
      $reflection_services = new ReflectionProperty('Wise\Container', 'services');
      $reflection_services->setAccessible(true);

      $container = new Container();
      $logger = new Logger();
      $reflection_services->setValue($container, array('log' => $logger));
      $service_1 = $container->getService('log');
      $service_2 = $container->getService('logger');

      $this->assertTrue(
        $service_1 instanceof Logger,
        'Failed to test that `getService()` correctly fetch the service searched.'
      );
      $this->assertTrue(
        $service_2 instanceof Logger,
        'Failed to test that `getService()` correctly fetch the service searched.'
      );
    }

    /**
     * Test for `getLogger()`.
     *
     * This test verifies that `getLogger()` correctly returns a `Logger` object
     * from the default services list.
     *
     * @since 1.0.0
     * @access public
     */
    public function testGetLogger() {
      $container = new Container();
      $logger = $container->getLogger();

      $this->assertTrue(
        is_a($logger, 'Wise\log\Logger'),
        'Failed to test that `getLogger()` correctly fetch the `Logger` searched.'
      );
    }
  }
