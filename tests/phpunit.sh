#!/usr/bin/env sh

main_test_path=.
cols=`tput cols`
unit_cols=`expr $cols - 12`
integration_cols=`expr $cols - 20`
validation_cols=`expr $cols - 18`

print_equal() {
	i=0
	until [ ! $i -lt $1 ]; do
		printf "="
		i=`expr $i + 1`
	done
}
print_line() {
	printf "\n"
	print_equal `expr $cols`
	printf "\n"
}

run_unit_test() {
	half_line=$(print_equal `expr $unit_cols / 2`)
	line=$(print_line)
	printf "$line$half_line Unit Tests $half_line$line"
	printf "\n"

	if [ -z "$1" ]; then
		../vendor/bin/phpunit -c unit.xml
	else
		../vendor/bin/phpunit -c unit.xml --filter $1
	fi
}
run_integration_test() {
	half_line=$(print_equal `expr $integration_cols / 2`)
	line=$(print_line)
	printf "$line$half_line Integration  Tests $half_line$line"
	printf "\n"

	if [ -z "$1" ]; then
		../vendor/bin/phpunit -c integration.xml
	else
		../vendor/bin/phpunit -c integration.xml --filter $1
	fi
}
run_validation_test() {
	half_line=$(print_equal `expr $validation_cols / 2`)
	line=$(print_line)
	printf "$line$half_line Validation Tests $half_line$line"
	printf "\n"

	if [ -z "$1" ]; then
		../vendor/bin/phpunit -c validation.xml
	else
		../vendor/bin/phpunit -c validation.xml --filter $1
	fi
}
run_all_tests() {
	run_unit_test $1
	printf "\n\n"
	run_integration_test $1
	printf "\n\n"
	run_validation_test $1
}

if [ -z "$1" ]; then
	run_all_tests
else
	if [ "$1" = "unit" ]; then
		run_unit_test $2
	fi
	if [ "$1" = "integration" ]; then
		run_integration_test $2
	fi
	if [ "$1" = "validation" ]; then
		run_validation_test $2
	fi
	if [ "$1" != "unit" ] && [ "$1" != "integration" ] && [ "$1" != "validation" ]; then
		run_all_tests $1
	fi
fi
