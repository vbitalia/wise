<?php
/**
 * Timer Helper APIs for Validation Tests.
 *
 * Copyright (C) 2015 VB Italia Srl
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  See LICENSE.md
 *
 * @package Wise\Tests\Validation
 * @copyright 2015 VB Italia Srl
 * @license GPLv2+
 * @author Mattia Migliorini <mattia@vbitalia.net>
 * @since 0.5.0
 */

namespace Wise\tests\validation;

use Wise\Container;
use Wise\log\LogLevel;

/**
 * Timer class.
 *
 * @package Wise\Tests\Validation
 * @author Mattia Migliorini <mattia@vbitalia.net>
 * @since 0.5.0
 * @version 1.1.0
 *
 * @codeCoverageIgnore
 */
class Timer {
	/**
	 * Time when timer started.
	 *
	 * @since 1.0.0
	 * @access protected
	 * @static
	 * @var float
	 */
	protected static $start = 0;

	/**
	 * Time when timer ended.
	 *
	 * @since 1.0.0
	 * @access protected
	 * @static
	 * @var float
	 */
	protected static $end = 0;

	/**
	 * Start timer.
	 *
	 * @since 1.1.0 Uses `microtime(true)` instead of `Timer::getMicroTime()`
	 * @since 1.0.0
	 * @access public
	 * @static
	 */
	public static function start() {
		self::$start = microtime(true);
	}

	/**
	 * Stop timer.
	 *
	 * @since 1.1.0 Uses `microtime(true)` instead of `Timer::getMicroTime()`
	 * @since 1.0.0
	 * @access public
	 * @static
	 */
	public static function stop() {
		self::$end = microtime(true);
	}

	/**
	 * Get time from timer.
	 *
	 * @since 1.0.0
	 * @access public
	 * @static
	 *
	 * @throws \RuntimeException if timer started but didn't end.
	 */
	public static function get() {
		if (! self::$end && self::$start)
			throw new \RuntimeException('You must stop the timer before getting time.');

		return self::$end - self::$start;
	}

	/**
	 * Reset timer.
	 *
	 * @since 1.0.0
	 * @access public
	 * @static
	 */
	public static function reset() {
		self::$start = self::$end = 0;
	}

	/**
	 * Get microtime as float.
	 *
	 * @deprecated 1.1.0 Use `microtime(true)` instead.
	 * @since 1.0.0
	 * @access private
	 * @static
	 *
	 * @return float Current microtime.
	 */
	private static function getMicroTime() {
		Container::getInstance()->getLogger()->deprecated(__METHOD__, '1.1.0', 'microtime(true)');

		return array_sum(explode(' ', microtime()));
	}
}
