<?php
/**
 * PHPUnit Bootstrap file for Validation Tests.
 *
 * Copyright (C) 2015 VB Italia Srl
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  See LICENSE.md
 *
 * @package Wise\Tests\Validation
 * @copyright 2015 VB Italia Srl
 * @author Mattia Migliorini <mattia@vbitalia.net>
 * @since 0.5.0
 */

namespace Wise;

// Increase floating point precision for better time tracking.
ini_set('precision', 16);

/**
 * Include Composer autoloader.
 */
require_once dirname(dirname(__DIR__)).'/vendor/autoload.php';
