<?php
/**
 * Validation test for class Wise.
 *
 * Copyright (C) 2015 VB Italia Srl
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  See LICENSE.md
 *
 * @package Wise\Tests\Validation
 * @copyright 2015 VB Italia Srl
 * @license GPLv2+
 * @author Mattia Migliorini <mattia@vbitalia.net>
 * @since 0.5.0
 */

namespace Wise\tests\validation;

use Wise\Wise;
use Wise\Priority;

/**
 * WiseTest class.
 *
 * @package Wise\Tests\Validation
 * @author Mattia Migliorini <mattia@vbitalia.net>
 * @since 0.5.0
 * @version 1.0.0
 */
class WiseTest extends \PHPUnit_Framework_TestCase {
	/**
	 * Instance of Wise.
	 *
	 * @since 1.0.0
	 * @access protected
	 * @static
	 * @var Wise
	 */
	protected static $wise;

	/**
	 * One-time setup.
	 *
	 * @since 1.0.0
	 * @access public
	 * @static
	 */
	public static function setUpBeforeClass() {
		self::$wise = new Wise(array(
			'coffees_taken' => 5,
			'code_lines' => 3
		));
	}

	/**
	 * Test 100 elements in a numeric array with `Wise::simpleSort()`.
	 *
	 * @since 1.0.0
	 * @access public
	 */
	public function testSortHundredSimpleNumericElements() {
		// Maximum expected time.
		$expected = 0.002;

		$actual = $this->execute(array('Wise\Wise', 'simpleSort'), array($this, 'generate'), array(100));

		$this->assertLessThan(
			$expected,
			$actual,
			sprintf(
				'Failed to assert that Wise::simpleSort() takes less than %f seconds to sort 100 elements in a numeric array.',
				$expected
			)
		);
	}

	/**
	 * Test 100 elements in an associative array with `Wise::simpleSort()`.
	 *
	 * @since 1.0.0
	 * @access public
	 */
	public function testSortHundredSimpleAssociativeElements() {
		// Maximum expected time.
		$expected = 0.002;

		$actual = $this->execute(array('Wise\Wise', 'simpleSort'), array($this, 'generate'), array(100, true));

		$this->assertLessThan(
			$expected,
			$actual,
			sprintf(
				'Failed to assert that Wise::simpleSort() takes less than %f seconds to sort 100 elements in an associative array.',
				$expected
			)
		);
	}

	/**
	 * Test 100 elements with modifiers without priorities with `Wise::sort()`.
	 *
	 * @since 1.0.0
	 * @access public
	 */
	public function testSortHundredComplexElements() {
		// Maximum expected time.
		$expected = 0.008;

		$actual = $this->execute(array(self::$wise, 'sort'), array($this, 'generateComplex'), array(100));

		$this->assertLessThan(
			$expected,
			$actual,
			sprintf(
				'Failed to assert that Wise::sort() takes less than %f seconds to sort 100 elements in a complex array without priorities.',
				$expected
			)
		);
	}

	/**
	 * Test 100 elements with modifiers and properties with `Wise::sort()`.
	 *
	 * @since 1.0.0
	 * @access public
	 */
	public function testSortHundredComplexElementsWithPriorities() {
		// Maximum expected time.
		$expected = 0.016;

		$actual = $this->execute(array(self::$wise, 'sort'), array($this, 'generateComplex'), array(100, true));

		$this->assertLessThan(
			$expected,
			$actual,
			sprintf(
				'Failed to assert that Wise::sort() takes less than %f seconds to sort 100 elements in a complex array with priorities.',
				$expected
			)
		);
	}

	/**
	 * Test 100 elements with modifiers and some with priorities with `Wise::sort()`.
	 *
	 * @since 1.0.0
	 * @access public
	 */
	public function testSortHundredComplexElementsMaybeWithPriorities() {
		// Maximum expected time.
		$expected = 0.015;

		$actual = $this->execute(
			array(self::$wise, 'sort'),
			array($this, 'generateComplex'),
			array(100, true, true)
		);

		$this->assertLessThan(
			$expected,
			$actual,
			sprintf(
				'Failed to assert that Wise::sort() takes less than %f seconds to sort 100 elements in a complex array maybe with priorities.',
				$expected
			)
		);
	}

	/**
	 * Execute the test and return time taken.
	 *
	 * @since 1.0.0
	 * @access private
	 *
	 * @param callback $callback Function to actually test.
	 * @param callback $generator Function to call to generate input.
	 * @param array $args Arguments to pass to the generator function.
	 * @return float Time taken for the test.
	 */
	private function execute($callback, $generator, $args) {
		$time = 0;

		for ($i = 0; $i < 50; $i++) {
			// Generate input.
			$elements = call_user_func_array($generator, $args);

			// Run the test.
			Timer::start();
			call_user_func($callback, $elements);
			Timer::stop();

			// Save execution time.
			$time += Timer::get();
		}

		return $time/50;
	}

	/**
	 * Generate a number of random elements.
	 *
	 * @since 1.0.0
	 * @access private
	 *
	 * @param int $num Number of elements to generate.
	 * @param bool $associative Optional. Whether to generate an associative array.
	 * Default false.
	 * @return array List of random elements.
	 */
	private function generate($num, $associative = false) {
		$elements = array();
		$keys = array();

		for ($i = 0; $i < $num; $i++) {
			if ($associative) {
				$key = (string)uniqid((string)$i);
				$elements[$key] = rand($i, $num*10);
			} else {
				$elements[] = rand($i, $num*10);
			}
		}

		return $elements;
	}

	/**
	 * Generate a number of random elements with modifiers and optionally priorities.
	 *
	 * @since 1.0.0
	 * @access private
	 *
	 * @param int $num Number of elements to generate.
	 * @param bool $priorities Optional. Whether to generate priorities. Default
	 * false.
	 * @param bool $random Optional. Whether to randomly generate priorities.
	 * Default false.
	 * @return array List of random elements.
	 */
	private function generateComplex($num, $priorities = false, $random = false) {
		$elements = array();
		$pri = Priority::getPriorities();

		for ($i = 0; $i < $num; $i++) {
			if ($random)
				$priorities = array_rand(array(false, true));

			$key = (string)uniqid((string)$i);
			if ($priorities) {
				$elements[$key] = array(
					'coffees_taken' => array(
						'value' => rand($i, $num*10),
						'priority' => $pri[array_rand($pri)]
					),
					'code_lines' => array(
						'value' => rand($i, $num*100),
						'priority' => $pri[array_rand($pri)]
					)
				);
			} else {
				$elements[$key] = array(
					'coffees_taken' => rand($i, $num*10),
					'code_lines' => rand($i, $num*100)
				);
			}
		}

		return $elements;
	}
}
