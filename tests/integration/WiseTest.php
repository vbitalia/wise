<?php
/**
 * Integration tests for the class Wise.
 *
 * Copyright (C) 2015 VB Italia Srl
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  See LICENSE.md
 *
 * @package Wise\Tests\Integration
 * @copyright 2015 VB Italia Srl
 * @author Gianluca Merlo <gianluca.merlo@vbitalia.net>
 * @license GPLv2+
 * @since 0.5.0
 */

namespace Wise\tests\integration;
use Wise\Wise;
use Wise\Tools;
use Wise\Priority;

 /**
  * Wise Integration Test class.
  *
  * This class contains the integration tests for the class `Wise`.
  *
  * @package Wise\Tests\Integration
  * @author Gianluca Merlo <gianluca.merlo@vbitalia.net>
  * @since 0.5.0
  * @version 0.5.0
  *
  * @coversNothing
  */
  class WiseTest extends \PHPUnit_Framework_TestCase {
    /**
     * Test for a simple array.
     *
     * This test verifies if a simple array is correctly sorted.
     *
     * @since 0.5.0
     * @access public
     */
    public function testSimpleArray() {
      $to_be_sorted = array();
      for ($i = 0; $i<100; $i++)
        $to_be_sorted[] = rand(0, 9999);

      $sorted = Wise::simpleSort($to_be_sorted);

      arsort($to_be_sorted);

      $this->assertTrue($sorted == array_keys($to_be_sorted), 'Failed to assert that the returned array has been correctly sorted.');
    }

    /**
     * Test for `sort()`.
     *
     * This test verifies if an array with modifiers and priorities is correctly
     * sorted.
     *
     * @since 0.5.0
     * @access public
     */
    public function testSort() {
      // Get the modifiers configuration from a json file.
      $modifiers = Tools::getJson(dirname(__DIR__).'/assets/json/modifiers.json');

      // Create a Wise object.
      $wise_test = new Wise($modifiers);

      // Create the arrays to sort.
      $programmer_of_the_week = array(
        "programmer_1" => array(
          'coffees_taken' => array(
            'value' => 12,
            'priority' => Priority::MEDIUM_HIGH
          ),
          'code_lines' => array(
            'value' => 2387,
            'priority' => Priority::MEDIUM_HIGH
          )
        ),
        "programmer_2" => array(
          'coffees_taken' => array(
            'value' => 19,
            'priority' => Priority::MEDIUM_LOW
          ),
          'code_lines' => 1452
        ),
        "programmer_3" => array(
          'coffees_taken' => array(
            'value' => 12
          ),
          'code_lines' => 1489
        ),
        "programmer_4" => array(
          'coffees_taken' => 17,
          'code_lines' => 2954
        ),
        "programmer_5" => array(
          'coffees_taken' => 2,
          'code_lines' => array(
            'value' => 5874,
            'priority' => Priority::LOW
          )
        ),
        "programmer_6" => array(
          'coffees_taken' => 37,
          'code_lines' => 4781
        ),
        "programmer_7" => array(
          'coffees_taken' => array(
            'value' => 3,
            'priority' => Priority::HIGH
          ),
          'code_lines' => array(
            'value' => 1256,
            'priority' => Priority::MEDIUM
          )
        ),
        "programmer_8" => array(
          'coffees_taken' => 16,
          'code_lines' => 2000
        ),
        "programmer_9" => array(
          'coffees_taken' => 12,
          'code_lines' => 894
        ),
        "programmer_10" => array(
          'coffees_taken' => 15,
          'code_lines' => 1206
        )
      );

      $actual_values = array(
        'programmer_6',
        'programmer_5',
        'programmer_4',
        'programmer_1',
        'programmer_8',
        'programmer_3',
        'programmer_2',
        'programmer_7',
        'programmer_10',
        'programmer_9'
      );

      // Calling the method with a correct array.
      $result = $wise_test->sort($programmer_of_the_week);

      $this->assertEquals(3, $modifiers['coffees_taken'], 'Test failed in fetching the configuration file of modifiers.');
      $this->assertEquals(5, $modifiers['code_lines'], 'Test failed in fetching the configuration file of modifiers.');
      $this->assertTrue($result == $actual_values, 'Test failed in sorting the array.');
    }

    /**
     * Test for `sort()`.
     *
     * This test verifies if `sort()` shows a warning when sorting a partially
     * incorrect array.
     *
     * @since 0.5.0
     * @access public
     *
     * @expectedException PHPUnit_Framework_Error_Warning
     */
    public function testSortWithPartiallyIncorrectArray() {
      // Get the modifiers configuration from a json file.
      $modifiers = Tools::getJson(dirname(__DIR__).'/assets/json/modifiers.json');

      // Create a Wise object.
      $wise_test = new Wise($modifiers);

      // Create the array to sort.
      $programmer_of_the_week = array(
        "programmer_1" => array(
          'coffees_taken' => array(
            'value' => 12,
            'priority' => Priority::MEDIUM_HIGH
          ),
          'code_lines' => array(
            'value' => 2387,
            'priority' => Priority::MEDIUM_HIGH
          )
        ),
        "programmer_2" => 12,
        "programmer_3" => array(
          'coffees_taken' => array(
            'priority' => 12
          ),
          'code_lines' => 1489
        ),
        "programmer_4" => array(
          'coffees_taken' => 17,
          'code_lines' => 2954
        ),
        "programmer_5" => array(
          'coffees_taken' => 2,
          'code_lines' => array(
            'value' => 5874,
            'priority' => Priority::LOW
          )
        ),
        "programmer_6" => array(
          'coffees_taken' => 37,
          'code_lines' => 4781
        ),
        "programmer_7" => array(
          'coffees_taken' => array(
            'value' => 3,
            'priority' => Priority::HIGH
          ),
          'code_lines' => array(
            'value' => 1256,
            'priority' => Priority::MEDIUM
          )
        ),
        "programmer_8" => array(
          'coffees_taken' => 16,
          'code_lines' => 2000
        ),
        "programmer_9" => array(
          'coffees_taken' => 12,
          'code_lines' => 894
        ),
        "programmer_10" => array()
      );

      $actual_values = array(
        'programmer_6',
        'programmer_5',
        'programmer_4',
        'programmer_1',
        'programmer_8',
        'programmer_3',
        'programmer_7',
        'programmer_9',
        'programmer_10'
      );

      // Calling the method with a partially incorrect array.
      $result = $wise_test->sort($programmer_of_the_week);
  }
}
