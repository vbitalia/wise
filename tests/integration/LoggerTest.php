<?php
/**
 * Integration tests for class Logger.
 *
 * Copyright (C) 2015 VB Italia Srl
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  See LICENSE.md
 *
 * @package Wise\Tests\Integration
 * @copyright 2015 VB Italia Srl
 * @author Mattia Migliorini <mattia@vbitalia.net>
 * @license GPLv2+
 * @since 0.6.0
 */

namespace Wise\tests\integration;

use Wise\log\Logger;

/**
 * Logger Integration Test class.
 *
 * @package Wise\Tests\Integration
 * @author Mattia Migliorini <mattia@vbitalia.net>
 * @since 0.6.0
 * @version 1.0.0
 *
 * @coversNothing
 */
class LoggerTest extends \PHPUnit_Framework_TestCase {
	/**
	 * Test `deprecated()` method without replacement.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @expectedException PHPUnit_Framework_Error_Deprecated
	 * @expectedExceptionMessage function is deprecated since 1.0.0.
	 */
	public function testDeprecatedWithoutReplacement() {
		$logger = new Logger;

		$logger->deprecated('function', '1.0.0');
	}

	/**
	 * Test `deprecated()` method with replacement.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @expectedException PHPUnit_Framework_Error_Deprecated
	 * @expectedExceptionMessage function is deprecated since 1.0.0. Use method instead.
	 */
	public function testDeprecatedWithReplacement() {
		$logger = new Logger;

		$logger->deprecated('function', '1.0.0', 'method');
	}
}
