<?php
/**
 * Wise Instant Sorting Engine.
 *
 * A smart products sorting algorithm based on modifiers, priorities and
 * attributes.
 *
 * Copyright (C) 2015 VB Italia Srl
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  See LICENSE.md
 *
 * @package Wise\Ecommerce
 * @copyright 2015 VB Italia Srl
 * @author Gianluca Merlo <gianluca.merlo@vbitalia.net>
 * @license GPLv2+
 * @since 0.6.0
 */

 namespace Wise\ecommerce;

 /**
  * WiseCommerce class.
  *
  * This class contains all the callable methods for sorting all your pretty
  * products.
  *
  * @package Wise\Ecommerce
  * @author Gianluca Merlo <gianluca.merlo@vbitalia.net>
  * @since 0.6.0
  * @version 0.1.0
  */
  interface WiseCommerceInterface {
    /**
     * Class constructor.
     *
     * Initializes `modifiers` and `user_modifiers`.
     *
     * @since 0.1.0
     * @access public
     *
     * @param array $modifiers Array of modifiers.
     * @param array $user_modifiers Array of user modifiers.
     */
    public function __construct(array $modifiers, array $user_modifiers = array());

    /**
     * Sort a list of products, considering modifiers, user modifiers,
     * attributes and priorities.
     *
     * This method sorts a list of products using modifiers, priorities and
     * attributes.
     * The priority given for a certain modifier of any product is used to raise
     * or lower the value of that specific modifier for that specific product
     * only, thus having the possibility to boost certain products through the
     * sorting process.
     * The products in `$user_products` array must be also in the `$products`
     * array.
     * Every `value` of each array given to the method must be an array in which
     * the keys can be modifiers or attributes.
     * Modifiers must match with the keys in `$modifiers` and `$user_modifiers`
     * respectively. For each one of these keys there can be a numeric value, or
     * another array. This array must have a key named 'value', with the
     * previous numeric value, and an additional key, called 'priority', that
     * must be one of the constants of the class `Priority`:
     * ['HIGH', 'MEDIUM_HIGH', 'MEDIUM', 'MEDIUM_LOW', 'LOW'].
     * Default is 'MEDIUM'.
     * Attributes can be everything that can play a part in the sorting process
     * (e.g. category, discount), and the value will be something related to
     * that attribute (e.g. 'category' => 'fitness'). The algorithm will take
     * into account this information when merging one array with the other.
     * If `$user_products` isn't passed to the method, it will only consider
     * `$products` in the sorting process, without caring abount attributes, so
     * attributes shouldn't be found in the `$products` array. If this happens
     * though, a warning will be showed.
     * The returned array will have numeric ascending keys, and the values that
     * correspond to the keys (the products) of the array passed to the method.
     *
     * @example https://bitbucket.org/snippets/vbitalia/98AER <3> <33>
     * How to use this method.
     *
     * @since 0.1.0
     * @access public
     *
     * @param array $products Associative array with string keys and array-type
     * values.
     * @param array $user_products Associative array with string keys and array-type
     * values.
     * @return array Ordered list of products.
     */
    public function sortProducts(array $products, array $user_products = array());
  }
