<?php
/**
 * Wise Instant Sorting Engine.
 *
 * A smart products sorting algorithm based on modifiers, priorities and
 * attributes.
 *
 * Copyright (C) 2015 VB Italia Srl
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  See LICENSE.md
 *
 * @package Wise\Ecommerce
 * @copyright 2015 VB Italia Srl
 * @author Gianluca Merlo <gianluca.merlo@vbitalia.net>
 * @license GPLv2+
 * @since 0.6.0
 */

 namespace Wise\ecommerce;

 use Wise\Wise;
 use Wise\Inquisitor;

 /**
  * WiseCommerce class.
  *
  * This class contains all the callable methods for sorting all your pretty
  * products.
  *
  * @package Wise
  * @author Gianluca Merlo <gianluca.merlo@vbitalia.net>
  * @since 0.6.0
  * @version 0.1.3
  */
  class WiseCommerce extends Wise implements WiseCommerceInterface {
    /**
     * List of user modifiers.
     * Associative array `"tag" => value` where `value` is numeric. `value`
     * must be a number between 0 and 10, zero excluded.
     *
     * @since 0.1.0
     * @access private
     * @var array
     */
    private $user_modifiers = array();

    /**
     * List of attributes.
     * Associative array `"attr_type" => "attribute"`.
     *
     * @since 0.1.0
     * @access private
     * @var array
     */
    private $attributes = array();

    /**
     * List of user products.
     * Associative array `"prod_name" => actual_value` where `actual_value` is
     * numeric.
     *
     * @since 0.1.0
     * @access private
     * @var array
     */
    private $user_prods = array();

    /**
     * Class constructor.
     *
     * {@inheritDoc }
     *
     * @since 0.1.3 No longer calls an `array_filter()`.
     * @since 0.1.0
     * @access public
     *
     * @param array $modifiers Array of modifiers.
     * @param array $user_modifiers Array of user modifiers.
     */
    public function __construct(array $modifiers, array $user_modifiers = array()) {
      parent::__construct($modifiers);
      $filtered_user_modifiers = Inquisitor::arrayFilter($user_modifiers, 'filterPositive');
      if (empty($filtered_user_modifiers))
        self::getLogger()->notice('No user modifiers passed to constructor, or at least no valid ones.');
      $this->user_modifiers = self::setModifiers($filtered_user_modifiers);
    }

    /**
     * Sort a list of products, considering modifiers, user modifiers,
     * attributes and priorities.
     *
     * {@inheritDoc }
     *
     * @since 0.1.1 Enables logger before returning the sorted products.
     * @since 0.1.0
     * @access public
     *
     * @param array $products Associative array with string keys and array-type
     * values.
     * @param array $user_products Associative array with string keys and array-type
     * values.
     * @return array Ordered list of products.
     */
    public function sortProducts(array $products, array $user_products = array()) {
      if (empty($products))
        return $products;

      if (empty($user_products)) {
        self::getLogger()->disable();
        $sorted = $this->sort($products);
        self::getLogger()->enable();
        return $sorted;
      }

      $filtered_prods = array_filter($user_products, 'is_array');

      // Check if the filtered array has less element than the original one.
      $diff = @array_diff_assoc($user_products, $filtered_prods);
      if (! empty($diff)) {
        self::getLogger()->warning(
          'Invalid data found in input: {diff}',
          array(
            'diff' => print_r($diff, true)
          ));
      }

      $this->user_prods = array_map(array($this, 'calculateUser'), $filtered_prods);

      // Calculate actual values of the products to sort...
      $to_be_sorted = array_map(array($this, 'calculateProduct'), array_keys($products), $products);

      // ...and sort them!
      arsort($to_be_sorted);
      return array_keys($to_be_sorted);
    }

    /**
     * Calculate the actual value of a user product.
     *
     * Calculate the actual value of a user product, and store the attributes
     * found in the object's property, or increment them if already present in
     * the property.
     *
     * @since 0.1.2 Call Inquisitor method directly instead of calling it as a
     * callback to `array_filter`.
     * @since 0.1.0
     * @access private
     *
     * @param array $user_product Associative array with string keys and
     * array-type or number or string values.
     * @return number The actual value of the product.
     */
    private function calculateUser(array $user_product) {
      $modifiers = array();
      $user_attributes = array();

      foreach ($user_product as $key => $val) {
        if (array_key_exists($key, $this->user_modifiers))
          $modifiers[$key] = $val;
        else
          $user_attributes[$key] = $val;
      }

      Inquisitor::setNeedles($this->user_modifiers);
      $filtered_modifiers = array();
      foreach ($modifiers as $k => $v) {
        if ($filtered_value = Inquisitor::filterKeyNeedles($v, $k))
          $filtered_modifiers[$k] = $filtered_value;
      }
      Inquisitor::unsetNeedles();

      $this->setAttributes($user_attributes);

      $actual = parent::calculate($filtered_modifiers);

      return $actual/10;
    }

    /**
     * Calculate the actual value of a product.
     *
     * Calculate the actual value of a product, using modifiers, priorities and
     * the attributes specified.
     *
     * @since 0.1.2 Call Inquisitor method directly instead of calling it as a
     * callback to `array_filter`.
     * @since 0.1.0
     * @access protected
     *
     * @param string $prod_name The identifier of the product.
     * @param array $product Associative array with string keys and array-type
     * or string or number values.
     * @return number The actual value of the product.
     */
    protected function calculateProduct($prod_name, $product) {
      if (! is_array($product))
        return 0;

      $modifiers = array();
      $attributes = array();

      foreach ($product as $key => $val) {
        if (array_key_exists($key, $this->modifiers))
          $modifiers[$key] = $val;
        else
          $attributes[$key] = $val;
      }

      Inquisitor::setNeedles($this->modifiers);
      $filtered_modifiers = array();
      foreach ($modifiers as $k => $v) {
        if ($filtered_value = Inquisitor::filterKeyNeedles($v, $k))
          $filtered_modifiers[$k] = $filtered_value;
      }
      Inquisitor::unsetNeedles();

      $actual = parent::calculate($filtered_modifiers);
      $actual *= $this->calculateAttributes($attributes);

      if (array_key_exists($prod_name, $this->user_prods))
        $actual *= $this->user_prods[$prod_name];

      return $actual;
    }

    /**
     * Set `attributes` property.
     *
     * Store attributes passed in the object's property, or increment them if
     * already stored.
     *
     * @since 0.1.0
     * @access private
     *
     * @param array $user_attributes Associative array with string keys and
     * string or (less-likely) numeric values.
     */
    private function setAttributes(array $user_attributes) {
      foreach ($user_attributes as $key => $val) {
        if (is_array($val))
          self::getLogger()->warning(
            'Invalid data found in input: {invalid}',
            array('invalid' => print_r(array($key => $val), true)));
        if (array_key_exists($key, $this->attributes)) {
          if (array_key_exists($val, $this->attributes[$key]))
            $this->attributes[$key][$val]++;
          else {
            $this->attributes[$key][$val] = 1;
          }
        }
        else
          $this->attributes[$key] = array($val => 1);
      }
    }

    /**
     * Calculate actual value of the attributes found in the product.
     *
     * @since 0.1.0
     * @access private
     *
     * @param array $attributes Associative array with string keys and string or
     * (less likely) numeric values.
     * @return number The actual value of the attributes.
     */
    private function calculateAttributes(array $attributes) {
      $actual_attributes = 1;

      foreach ($attributes as $key => $val) {
        if (array_key_exists($key, $this->attributes) && array_key_exists($val, $this->attributes[$key]))
          $actual_attributes *= $this->attributes[$key][$val];
      }

      return $actual_attributes;
    }
  }
