<?php
/**
 * Wise Instant Sorting Engine.
 *
 * A smart sorting algorithm based on modifiers and priorities.
 *
 * Copyright (C) 2015 VB Italia Srl
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  See LICENSE.md
 *
 * @package Wise
 * @copyright 2015 VB Italia Srl
 * @author Gianluca Merlo <gianluca.merlo@vbitalia.net>
 * @license GPLv2+
 * @since 0.1.0
 */

 namespace Wise;

 use Wise\Inquisitor;
 use Wise\Container;

 /**
  * WISE main class.
  *
  * This class contains all the callable methods for sorting all your pretty
  * stuff.
  *
  * @package Wise
  * @author Gianluca Merlo <gianluca.merlo@vbitalia.net>
  * @since 0.1.0
  * @version 0.4.1
  */
  class Wise implements WiseInterface {
    /**
     * List of modifiers.
     * Associative array `"tag" => value` where `value` is numeric. `value`
     * must be a number between 0 and 10, zero excluded.
     *
     * @since 0.1.0
     * @access protected
     * @var array
     */
    protected $modifiers = array();

    /**
     * Constructor.
     *
     * {@inheritDoc }
     *
     * @since 0.4.1 No longer calls an `array_filter()`.
     * @since 0.1.0
     * @access public
     *
     * @param array $modifiers Array of modifiers.
     */
    public function __construct(array $modifiers) {
      $filtered_modifiers = Inquisitor::arrayFilter($modifiers, 'filterPositive');
      if (empty($filtered_modifiers)) {
        self::getLogger()->notice('No modifiers passed to constructor, or at least no valid ones.');
      }
      $this->modifiers = self::setModifiers($filtered_modifiers);
    }

    /**
     * Order a list considering modifiers and priorities.
     *
     * {@inheritDoc }
     *
     * @example https://bitbucket.org/snippets/vbitalia/98AER <3> <33>
     * <How to use this method>
     *
     * @since 0.4.0
     * @access public
     *
     * @param array $objects Associative array with string keys and array-type
     * values.
     * @return array Ordered list of elements.
     */
    public function sort(array $objects) {
      // Check if `$objects` is empty.
      if (empty($objects))
        return $objects;

      $filtered_objects = Inquisitor::sanitizeArray($objects, array_keys($this->modifiers));

      // Check if the filtered array has less element than the original one.
      $diff = @array_diff_assoc($objects, $filtered_objects);
      if (! empty($diff)) {
        $logger = self::getLogger();
        $logger->warning(
          'Invalid data found in input: {diff}',
          array(
            'diff' => print_r($diff, true)
          ));
      }

      // Calculate actual values to sort...
      $to_be_sorted = array_map(array($this, 'calculate'), $filtered_objects);

      // ...and sort them!
      arsort($to_be_sorted);
      return array_keys($to_be_sorted);
    }

    /**
     * Sort a simple array.
     *
     * {@inheritDoc }
     *
     * @since 0.3.0
     * @access public
     * @static
     *
     * @param array $objects Associative array with string keys and numeric
     * values.
     * @return array Ordered list of elements.
     */
    public static function simpleSort(array $objects) {
      // Check if `$objects` is empty.
      if (empty($objects))
        return $objects;

      $filtered_objects = Inquisitor::sanitizeArray($objects);

      // Check if the filtered array has less element than the original one.
      $diff = array_diff_assoc($objects, $filtered_objects);
      if (! empty($diff)) {
        $logger = self::getLogger();
        $logger->warning(
          'Invalid data found in input: {diff}',
          array(
            'diff' => print_r($diff, true)
          ));
      }

      arsort($filtered_objects);
      return array_keys($filtered_objects);
    }

    /**
     * Change `value` in `$modifiers` to be in the correct form.
     *
     * Check if `value` in `$modifiers` are in the correct form and return true
     * if they are. Otherwise returns an array of correct modifiers list.
     *
     * @since 0.1.0
     * @access protected
     * @static
     *
     * @param array $modifiers The list to be checked.
     * @return array Array with the correct values.
     */
    protected static function setModifiers($modifiers) {
      // Check if `$modifiers` is an empty array. If true, returns it without
      // doing any calculation.
      if (empty($modifiers))
        return $modifiers;

      // Initialize variables to compare the elements of the array between them.
      $measure = max($modifiers);
      $divisions = 0;
      while ($measure > 10) {
        $measure = $measure/10;
        $divisions++;
      }

      // Check if the array is in the correct form.
      if ($divisions == 0)
        return $modifiers;

      // Fix the array and return it.
      foreach ($modifiers as &$modifier) {
        $count = 0;
        while ($count < $divisions) {
          $modifier = $modifier/10;
          $count++;
        }
      }
      return $modifiers;
    }

    /**
     * Calculate actual value.
     *
     * This method calculate the actual value of every element, that will be
     * used to sort the array.
     * These calculation are done considering the list of modifiers and the
     * priority, if specified.
     *
     * @since 0.4.0
     * @access protected
     *
     * @param array $element The array with the values used in the calculation
     * of the actual value.
     * @return number The actual value that will be used to sort the array.
     */
    protected function calculate(array $element) {
      // Calculate the actual value.
      $actual = 0;
      foreach ($element as $key => $value) {
        $modifier = $this->modifiers[$key];
        if (! is_array($value))
          $actual += $value * $modifier;
        else {
          if (array_key_exists('priority', $value))
            $modifier += $value['priority'];
          else
            $modifier += Priority::_DEFAULT_;
          $actual += $value['value'] * $modifier;
        }
      }

      return $actual;
    }

    /**
     * Get the `Container` instance.
     *
     * @since 0.5.0
     * @access protected
     * @static
     *
     * @return Container The instance of the Container.
     *
     * @codeCoverageIgnore
     */
    protected static function getContainer() {
      return Container::getInstance();
    }

    /**
     * Get the `Logger` object saved in `Container`.
     *
     * @since 0.5.0
     * @access protected
     * @static
     *
     * @return Logger
     *
     * @codeCoverageIgnore
     */
    protected static function getLogger() {
      $container = self::getContainer();
      return $container->getLogger();
    }

    /**
     * Ternary Merge Sort.
     *
     * Sort an array dividing it into three approximately equal sub-arrays, and
     * calling itself recursively passing them as parameters. If necessary,
     * calls another method to sort the sub-arrays.
     *
     * @deprecated 0.4.0 No longer used by internal code and not recommended.
     *
     * @since 0.3.0
     * @access protected
     * @static
     *
     * @param array $objects Associative array with string keys and numeric
     * values.
     * @return array Ordered list of elements.
     *
     * @codeCoverageIgnore
     */
    protected static function ternaryMergeSort(array $objects) {
      self::getLogger()->deprecated(__METHOD__, '0.4.0', 'asort');

      $length = count($objects);
      if ($length < 2)
        return $objects;
      elseif ($length <= 1000000) {
        asort($objects);
        return $objects;
      } elseif ($length <= 5000000) {
        return self::mergeSort($objects);
      }

      // Split `$objects` into three chunks.
      $chunk_length = ceil($length/3);
      $chunks = array_chunk($objects, $chunk_length, true);

      // Call itself (or the most convenient method) on every chunk.
      $chunks[0] = self::ternaryMergeSort($chunks[0]);
      $chunks[1] = self::ternaryMergeSort($chunks[1]);
      $chunks[2] = self::ternaryMergeSort($chunks[2]);

      // Merge the chunks.
      $left = true;
      $center = true;
      $right = true;
      $sorted = array();

      while ($left || $center || $right) {
        // Get first element (and it's `value`) of each chunk.
        $left_element = array();
        $center_element = array();
        $right_element = array();
        $left_element_value = PHP_INT_MAX;
        $center_element_value = PHP_INT_MAX;
        $right_element_value = PHP_INT_MAX;
        if ($left) {
          $left_element = each($chunks[0]);
          $left_element_value = $left_element[1];
        }
        if ($center) {
          $center_element = each($chunks[1]);
          $center_element_value = $center_element[1];
        }
        if ($right) {
          $right_element = each($chunks[2]);
          $right_element_value = $right_element[1];
        }

        // Find lowest `value`.
        $min_value = min($left_element_value, $center_element_value, $right_element_value);

        // Insert lowest `value` and corresponding `key` into `$sorted`, than
        // rewind the internal array pointer of the other chunks.
        if ($left_element_value == $min_value) {
          $sorted[$left_element[0]] = $left_element_value;
          prev($chunks[1]);
          prev($chunks[2]);
          if (! next($chunks[0]))
            $left = false;
        }
        else {
          if ($center_element_value == $min_value) {
            $sorted[$center_element[0]] = $center_element_value;
            prev($chunks[0]);
            prev($chunks[2]);
            if (! next($chunks[1]))
              $center = false;
          }
          else {
            if ($right_element_value == $min_value) {
              $sorted[$right_element[0]] = $right_element_value;
              prev($chunks[0]);
              prev($chunks[1]);
              if (! next($chunks[2]))
                $right = false;
            }
          }
        }
      }
      return $sorted;
    }

    /**
     * Merge Sort.
     *
     * Sort an array dividing it into two approximately equal sub-arrays, and
     * calling itself recursively passing them as parameters. If necessary,
     * calls another method to sort the sub-arrays.
     *
     * @deprecated 0.4.0 No longer used by internal code and not recommended.
     *
     * @since 0.3.0
     * @access protected
     * @static
     *
     * @param array $objects Associative array with string keys and numeric
     * values.
     * @return array Ordered list of elements.
     *
     * @codeCoverageIgnore
     */
    protected static function mergeSort(array $objects) {
      self::getLogger()->deprecated(__METHOD__, '0.4.0', 'asort');

      $length = count($objects);
      if ($length < 2)
        return $objects;
      if ($length <= 1000000) {
        asort($objects);
        return $objects;
      }

      // Split `$objects` into two chunks.
      $chunk_length = ceil($length/2);
      $chunks = array_chunk($objects, $chunk_length, true);

      // Call itself (or the most convenient method) on every chunk.
      $chunks[0] = self::mergeSort($chunks[0]);
      $chunks[1] = self::mergeSort($chunks[1]);

      // Merge the chunks.
      $left = true;
      $right = true;
      $sorted = array();

      while ($left || $right) {
        // Get first element (and it's `value`) of each chunk.
        $left_element = array();
        $right_element = array();
        $left_element_value = PHP_INT_MAX;
        $right_element_value = PHP_INT_MAX;
        if ($left) {
          $left_element = each($chunks[0]);
          $left_element_value = $left_element[1];
        }
        if ($right) {
          $right_element = each($chunks[1]);
          $right_element_value = $right_element[1];
        }

        // Find lowest `value`.
        $min_value = min($left_element_value, $right_element_value);

        // Insert lowest `value` and corresponding `key` into `$sorted`, than
        // rewind the internal array pointer of the other chunks.
        if ($left_element_value == $min_value) {
          $sorted[$left_element[0]] = $left_element_value;
          prev($chunks[1]);
          if (! next($chunks[0]))
            $left = false;
        }
        else {
          if ($right_element_value == $min_value) {
            $sorted[$right_element[0]] = $right_element_value;
            prev($chunks[0]);
            if (! next($chunks[1]))
              $right = false;
          }
        }
      }
      return $sorted;
    }

    /**
     * Insertion Sort.
     *
     * Sort an array by taking into account one element at a time, and moving it
     * to his place between the elements before it.
     *
     * @deprecated 0.4.0 No longer used by internal code and not recommended.
     *
     * @since 0.3.0
     * @access protected
     * @static
     *
     * @param array $objects Associative array with string keys and numeric
     * values.
     * @return array Ordered list of elements.
     *
     * @codeCoverageIgnore
     */
    protected static function insertSort(array $objects) {
      self::getLogger()->deprecated(__METHOD__, '0.4.0', 'asort');

      $length = count($objects);
      if ($length < 2)
        return $objects;

      // Sort elements in the array.
      $sorted = array();
      $first_element = each($objects);
      $sorted[$first_element[0]] = $first_element[1];
      array_shift($objects);
      foreach ($objects as $key => $value) {
        $count = 0;
        foreach ($sorted as $s_key => $s_value) {
          if ($value < $s_value)
            break;
          $count++;
        }
        $spliced = array_splice($sorted, $count);
        $sorted[$key] = $value;
        $sorted = array_merge($sorted, $spliced);
      }

      return $sorted;
    }
  }
